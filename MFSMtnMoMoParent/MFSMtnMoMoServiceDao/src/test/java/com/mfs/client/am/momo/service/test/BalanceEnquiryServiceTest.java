package com.mfs.client.am.momo.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.mtn.momo.dto.BalanceResponseDto;
import com.mfs.client.mtn.momo.exception.DaoException;
import com.mfs.client.mtn.momo.service.BalanceEnquiryService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class BalanceEnquiryServiceTest {

	@Autowired
	BalanceEnquiryService blanceEnquiryService;

	// For Success
	@Ignore
	@Test
	public void balanceEnquirySuccessTest() throws DaoException {

		BalanceResponseDto balanceResponseDto = blanceEnquiryService.balanceService("ZM");
		System.out.println(balanceResponseDto);

		Assert.assertNotNull(balanceResponseDto.getAvailableBalance());
	}

}
