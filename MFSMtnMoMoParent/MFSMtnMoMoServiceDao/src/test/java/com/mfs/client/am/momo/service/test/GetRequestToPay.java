package com.mfs.client.am.momo.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.mtn.momo.dto.GetRequestToPayResponseDto;
import com.mfs.client.mtn.momo.service.GetRequestToPayService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class GetRequestToPay {

	@Autowired
	GetRequestToPayService getRequestToPayService;

	// Check for request to pay success response
	@Ignore
	@Test
	public void getRequestToPayServiceSuccessTest() {

		GetRequestToPayResponseDto responseDto = getRequestToPayService.getRequestToPay("MFSTEST003");
		System.out.println(responseDto);
		Assert.assertNotNull(responseDto.getStatus());

	}

}
