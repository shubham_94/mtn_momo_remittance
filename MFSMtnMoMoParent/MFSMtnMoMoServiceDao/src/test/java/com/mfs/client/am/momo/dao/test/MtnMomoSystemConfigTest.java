package com.mfs.client.am.momo.dao.test;

import java.util.Map;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.mtn.momo.dao.MtnMoMoSystemConfigDao;
import com.mfs.client.mtn.momo.exception.DaoException;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class MtnMomoSystemConfigTest {

	@Autowired
	MtnMoMoSystemConfigDao mtnMoMoSystemConfigDao;

	@Ignore
	@Test
	public void systemConfigTest() throws DaoException {

		Map<String, String> configMap = mtnMoMoSystemConfigDao.getConfigDetailsMap();
		
		Assert.assertNotNull(configMap);

	}

}
