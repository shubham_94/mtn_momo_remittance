package com.mfs.client.am.momo.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.mtn.momo.dto.UserRequestDto;
import com.mfs.client.mtn.momo.dto.UserResponse;
import com.mfs.client.mtn.momo.service.UserService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class UserServiceTest {
	@Autowired
	UserService userService;

	@Ignore
	@Test
	public void UserServiceSuccessTest() throws Exception {
		UserRequestDto request = new UserRequestDto();
		request.setProviderCallbackHost("www.google.com");

		UserResponse response = userService.userService(request);
		System.out.println(response);

		Assert.assertEquals("201", response.getCode());
	}

	@Ignore
	@Test
	public void UserServiceCheckFailResponse() throws Exception {
		UserRequestDto request = new UserRequestDto();
		request.setProviderCallbackHost(null);

		UserResponse response = userService.userService(request);
		System.out.println(response);

		Assert.assertEquals("ER400", response.getCode());
	}
}