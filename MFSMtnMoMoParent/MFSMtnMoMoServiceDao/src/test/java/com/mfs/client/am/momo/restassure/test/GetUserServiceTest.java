package com.mfs.client.am.momo.restassure.test;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.http.ContentType;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class GetUserServiceTest {
	// Success
	@Ignore
	@Test
	public void getUserServiceTest() {

		given().body("").contentType(ContentType.JSON).when().get("http://localhost:8080/MFSMtnMoMoWeb/getuserapi")
				.then().statusCode(equalTo(HttpStatus.OK.value())).body("targetEnvironment", is("sandbox"));
	}

	// Validation
	@Ignore
	@Test
	public void GetUserServiceTestNotNull() {

		given().body("").contentType(ContentType.JSON).when().get("http://localhost:8080/MFSMtnMoMoWeb/getuserapi")
				.then().statusCode(equalTo(HttpStatus.OK.value())).body("targetEnvironment", not(nullValue()));
	}

}
