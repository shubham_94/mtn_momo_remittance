package com.mfs.client.am.momo.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.mtn.momo.dto.GetUserResponseDto;
import com.mfs.client.mtn.momo.service.GetUserService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class GetUserServiceTest {
	
	@Autowired
	GetUserService getUserService;
	
	//For Success
	@Ignore
	@Test
	public void getServiceUserTest()
	{
		GetUserResponseDto response= getUserService.getUserService();
		System.out.println(response);

		Assert.assertEquals("sandbox", response.getTargetEnvironment());
	}
	
	

}
