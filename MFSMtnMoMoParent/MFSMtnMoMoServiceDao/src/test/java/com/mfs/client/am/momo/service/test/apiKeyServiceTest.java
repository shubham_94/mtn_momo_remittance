package com.mfs.client.am.momo.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.mtn.momo.dao.TransactionDao;
import com.mfs.client.mtn.momo.dto.ApiKeyResponseDto;
import com.mfs.client.mtn.momo.dto.UserRequestDto;
import com.mfs.client.mtn.momo.service.ApiKeyService;
import com.mfs.client.mtn.momo.service.UserService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class apiKeyServiceTest {

	@Autowired
	ApiKeyService apiKeyService;

	@Autowired
	UserService userService;

	@Autowired
	TransactionDao transactionDao;

	@Ignore
	@Test
	public void apiKeySuccessTest() throws Exception {

		UserRequestDto requestDto = new UserRequestDto();
		requestDto.setProviderCallbackHost("google.com");

		ApiKeyResponseDto response = apiKeyService.createApiKey();
		System.out.println(response);

		Assert.assertNotNull(response.getApiKey());
	}

}
