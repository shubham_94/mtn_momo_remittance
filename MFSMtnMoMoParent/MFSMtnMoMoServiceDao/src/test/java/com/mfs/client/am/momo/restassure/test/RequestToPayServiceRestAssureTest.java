package com.mfs.client.am.momo.restassure.test;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Ignore;
import org.springframework.http.HttpStatus;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.http.ContentType;
import com.mfs.client.mtn.momo.dto.Payee;
import com.mfs.client.mtn.momo.dto.RequestToPayRequestDto;
import com.mfs.client.mtn.momo.service.RequestToPayService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class RequestToPayServiceRestAssureTest {

	@Autowired
	RequestToPayService requestToPayService;

	// This for successfully service work
	@Ignore
	@Test
	public void requestToPayServicesTest() {

		RequestToPayRequestDto requestDto = new RequestToPayRequestDto();

		requestDto.setAmount("500000");
		requestDto.setCurrency("INR");
		requestDto.setExternalId("6478");

		Payee payee = new Payee();

		payee.setPartyIdType("123");
		payee.setPartyId("123");

		requestDto.setPayee(payee);
		requestDto.setPayerMessage("Success");
		requestDto.setPayeeNote("qwerty");

		given().body(requestDto).contentType(ContentType.JSON).when()
				.post("http://localhost:8080/MFSMtnMoMoWeb/requesttopay").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("code", is("200"));

	}

	// This for null validation
	@Ignore
	@Test
	public void requestToPayServicesNotNullTest() {

		RequestToPayRequestDto requestDto = new RequestToPayRequestDto();

		requestDto.setAmount("");
		requestDto.setCurrency("INR");
		requestDto.setExternalId("6478");

		Payee payee = new Payee();

		payee.setPartyIdType("123");
		payee.setPartyId("123");

		requestDto.setPayee(payee);
		requestDto.setPayerMessage("Success");
		requestDto.setPayeeNote("qwerty");

		given().body(requestDto).contentType(ContentType.JSON).when()
				.post("http://localhost:8080/MFSMtnMoMoWeb/requesttopay").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("code", is("ER206"));

	}

	// This for fail
	@Ignore
	@Test
	public void requestToPayServicesFailTest() {

		RequestToPayRequestDto requestDto = new RequestToPayRequestDto();

		requestDto.setAmount("500000");
		requestDto.setCurrency("INR");
		requestDto.setExternalId("6478");

		Payee payee = new Payee();

		payee.setPartyIdType("123");
		payee.setPartyId("123");

		requestDto.setPayee(payee);
		requestDto.setPayerMessage("Success");
		requestDto.setPayeeNote("qwerty");

		given().body(requestDto).contentType(ContentType.JSON).when()
				.post("http://localhost:8080/MFSMtnMoMoWeb/requesttopay").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("code", is("ER210"));

	}

}
