package com.mfs.client.am.momo.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.mtn.momo.dto.TokenResponseDto;
import com.mfs.client.mtn.momo.service.AuthorizationTokenService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class AuthorizationTokenServiceTest {
	@Autowired
	AuthorizationTokenService authorizationTokenService;

	// For Success
	@Ignore
	@Test
	public void authorizationTokenServiceTest() throws Exception {

		TokenResponseDto response = authorizationTokenService.createToken("ZM");
		System.out.println(response);
		Assert.assertEquals("access_token", response.getToken_type());
	}

}
