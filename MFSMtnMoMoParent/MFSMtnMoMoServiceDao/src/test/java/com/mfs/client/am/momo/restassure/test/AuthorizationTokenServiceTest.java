package com.mfs.client.am.momo.restassure.test;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.http.ContentType;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class AuthorizationTokenServiceTest {

	@Ignore
	@Test
	public void authorizationTokenServiceTest() {

		given().body("").contentType(ContentType.JSON).when().post("http://localhost:8080/MFSMtnMoMoWeb/token").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("token_type", is("access_token"));
	}

	@Ignore
	@Test
	public void authorizationTokenServiceNotNullTest() {

		given().body("").contentType(ContentType.JSON).when().post("http://localhost:8080/MFSMtnMoMoWeb/token").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("token_type", not(nullValue()));
	}

	@Ignore
	@Test
	public void authorizationTokenServiceFailTest() {

		given().body("").contentType(ContentType.JSON).when().post("http://localhost:8080/MFSMtnMoMoWeb/token").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("code", is("ER401"));
	}

}
