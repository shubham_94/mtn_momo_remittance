package com.mfs.client.am.momo.restassure.test;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.ValidatableResponse;
import com.mfs.client.mtn.momo.dao.TransactionDao;
import com.mfs.client.mtn.momo.dto.UserRequestDto;
import com.mfs.client.mtn.momo.exception.DaoException;
import com.mfs.client.mtn.momo.service.ApiKeyService;
import com.mfs.client.mtn.momo.service.UserService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class ApiKeyServiceRestAssureTest {

	@Autowired
	ApiKeyService apiKeyService;

	@Autowired
	UserService userService;

	@Autowired
	TransactionDao transactionDao;

	@Ignore
	@Test
	public void apiKeyTest() throws DaoException {

		UserRequestDto requestDto = new UserRequestDto();
		requestDto.setProviderCallbackHost("google.com");

		given().body("").contentType(ContentType.JSON).when().post("http://localhost:8080/MFSMtnMoMoWeb/apikey").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("apiKey", not(nullValue()));
	}

	@Ignore
	@Test
	public void apiKeyvalidationTest() {

		ValidatableResponse response = given().body("").contentType(ContentType.JSON).when()
				.post("http://localhost:8080/MFSMtnMoMoWeb/apikey").then().statusCode(equalTo(HttpStatus.OK.value()))
				.body("statusCode", is("ER216"));
	}

}
