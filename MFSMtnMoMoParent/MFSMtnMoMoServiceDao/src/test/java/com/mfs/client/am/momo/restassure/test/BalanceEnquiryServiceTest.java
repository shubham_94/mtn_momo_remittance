package com.mfs.client.am.momo.restassure.test;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.http.ContentType;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class BalanceEnquiryServiceTest {

	// Success
	@Ignore
	@Test
	public void balanceEnquiryServiceTest() {

		given().body("").contentType(ContentType.JSON).when().get("http://localhost:8080/MFSMtnMoMoWeb/getbalanceapi")
				.then().statusCode(equalTo(HttpStatus.OK.value())).body("currency", is("EUR"));
	}

	// Validation
	@Ignore
	@Test
	public void balanceEnquiryServiceNotNull() {

		given().body("").contentType(ContentType.JSON).when().get("http://localhost:8080/MFSMtnMoMoWeb/getbalanceapi")
				.then().statusCode(equalTo(HttpStatus.OK.value())).body("currency", not(nullValue()));
	}

	// Fail
	@Ignore
	@Test
	public void balanceEnquiryServiceFailTest() {

		given().body("").contentType(ContentType.JSON).when().get("http://localhost:8080/MFSMtnMoMoWeb/getbalanceapi")
				.then().statusCode(equalTo(HttpStatus.OK.value())).body("code", is("ER201"));
	}
}
