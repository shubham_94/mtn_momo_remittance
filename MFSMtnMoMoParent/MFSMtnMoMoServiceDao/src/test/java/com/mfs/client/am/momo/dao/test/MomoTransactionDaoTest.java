package com.mfs.client.am.momo.dao.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.mtn.momo.dao.TransactionDao;
import com.mfs.client.mtn.momo.exception.DaoException;
import com.mfs.client.mtn.momo.models.UserModel;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class MomoTransactionDaoTest {

	@Autowired
	TransactionDao transactionDao;

	@Ignore
	@Test
	public void userServiceByreferenceId() throws DaoException {
		UserModel userModel = transactionDao.getTransactionLogByreferenceId("280a8360-052e-417d-98a9-84c89995e251");

		Assert.assertNotNull(userModel);
	}

	@Ignore
	@Test
	public void userServiceByreferenceIdEmpty() throws DaoException {
		UserModel userModel = transactionDao.getTransactionLogByreferenceId("12312e");
		
		Assert.assertNull(userModel);
	}

	@Ignore
	@Test
	public void getUserServiceLastRecordByreferenceId() throws DaoException {
		UserModel userModel = transactionDao.getLastRecordByreferenceId();
		
		Assert.assertNotNull(userModel);
	}

	@Ignore
	@Test
	public void getUserServiceLastRecordByreferenceIdEmpty() throws DaoException {
		UserModel userModel = transactionDao.getLastRecordByreferenceId();
		
		Assert.assertNull(userModel);
	}
}
