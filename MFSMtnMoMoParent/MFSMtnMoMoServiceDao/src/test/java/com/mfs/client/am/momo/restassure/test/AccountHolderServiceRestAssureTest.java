package com.mfs.client.am.momo.restassure.test;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.http.ContentType;
import com.mfs.client.mtn.momo.dto.ValidateAccountRequestDto;
import com.mfs.client.mtn.momo.service.AccountHolderService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class AccountHolderServiceRestAssureTest {

	@Autowired
	AccountHolderService accountHolderService;

	
	// This for Success
	@Ignore
	@Test
	public void accountHolderServiceTest() {

		ValidateAccountRequestDto request = new ValidateAccountRequestDto();
		request.setAccountHolderIdType("msisdn");
		request.setAccountHolderId("9131144608");
		given().body(request).contentType(ContentType.JSON).when()
				.post("http://localhost:8080/MFSMtnMoMoWeb/getaccountholder").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("result", is("true"));

	}
	
	@Ignore
	@Test
	public void accountHolderServiceTestWithErrorcode() {
		ValidateAccountRequestDto request = new ValidateAccountRequestDto();
		request.setAccountHolderIdType("msisdn");
		request.setAccountHolderId("9131144608");
		given().body(request).contentType(ContentType.JSON).when()
				.post("http://localhost:8080/MFSMtnMoMoWeb/getaccountholder").then().statusCode(equalTo(HttpStatus.OK.value()))
				.body("code", is("ER216"));
	}
	
	@Ignore
	@Test
	public void accountHolderServiceTestForNotNull() {

		ValidateAccountRequestDto request = new ValidateAccountRequestDto();
		request.setAccountHolderIdType("msisdn");
		request.setAccountHolderId("9131144608");
		given().body(request).contentType(ContentType.JSON).when()
				.post("http://localhost:8080/MFSMtnMoMoWeb/getaccountholder").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("result", not(nullValue()));

	}
	
	@Ignore
	@Test
	public void accountHolderServiceFailValid() {

		ValidateAccountRequestDto request = new ValidateAccountRequestDto();
		request.setAccountHolderIdType("msisdn");
		request.setAccountHolderId("9131144608");
		given().body(request).contentType(ContentType.JSON).when()
				.post("http://localhost:8080/MFSMtnMoMoWeb/getaccountholder").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("code", is("ER211"));

	}

}
