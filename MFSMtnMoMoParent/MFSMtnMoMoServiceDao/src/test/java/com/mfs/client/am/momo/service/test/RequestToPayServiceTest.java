package com.mfs.client.am.momo.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.mtn.momo.dto.Payee;
import com.mfs.client.mtn.momo.dto.RequestToPayRequestDto;
import com.mfs.client.mtn.momo.dto.RequestToPayResponseDto;
import com.mfs.client.mtn.momo.service.RequestToPayService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class RequestToPayServiceTest {

	@Autowired
	RequestToPayService requestToPayService;

	// Check for request to pay success response
	@Ignore
	@Test
	public void requestToPayServiceSuccessTest() {

		RequestToPayRequestDto requestDto = new RequestToPayRequestDto();

		requestDto.setAmount("100");
		requestDto.setCurrency("EUR");
		requestDto.setExternalId("MFSTEST003");

		Payee payee = new Payee();

		payee.setPartyId("8889992325");

		requestDto.setPayee(payee);
		requestDto.setPayerMessage("BusinessPayment");
		requestDto.setPayeeNote("BusinessPayment");
		requestDto.setCountryCode("ZM");

		RequestToPayResponseDto responseDto = requestToPayService.requestToPay(requestDto);
		System.out.println(responseDto);
		Assert.assertEquals("202", responseDto.getCode());

	}

	// check for request to pay duplicate response
	@Ignore
	@Test
	public void RequestToPayTestDuplicateExternalId() throws Exception {
		RequestToPayRequestDto requestDto = new RequestToPayRequestDto();

		requestDto.setAmount("100");
		requestDto.setCurrency("EUR");
		requestDto.setExternalId("MFSTEST002");

		Payee payee = new Payee();

		payee.setPartyId("8889992325");

		requestDto.setPayee(payee);
		requestDto.setPayerMessage("BusinessPayment");
		requestDto.setPayeeNote("BusinessPayment");
		requestDto.setCountryCode("ZM");

		RequestToPayResponseDto responseDto = requestToPayService.requestToPay(requestDto);
		System.out.println(responseDto);
		Assert.assertEquals("ER210", responseDto.getCode());
	}

}
