package com.mfs.client.am.momo.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.mtn.momo.dto.ValidateAccountResponseDto;
import com.mfs.client.mtn.momo.service.AccountHolderService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class AccountHolderServiceTest {

	@Autowired
	AccountHolderService accountHolderService;

	// For Success
	@Ignore
	@Test
	public void accountHolderServiceTest() throws Exception {
		ValidateAccountResponseDto response = accountHolderService.getAccountHolderService("8889992325", "ZM");
		System.out.println(response);
		Assert.assertNotNull(response.getName());
	}

}
