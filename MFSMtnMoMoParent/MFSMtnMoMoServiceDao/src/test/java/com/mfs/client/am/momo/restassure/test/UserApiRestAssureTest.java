package com.mfs.client.am.momo.restassure.test;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.http.ContentType;
import com.mfs.client.mtn.momo.dto.UserRequestDto;
import com.mfs.client.mtn.momo.service.UserService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class UserApiRestAssureTest {

	@Autowired
	UserService userService;
	
	//Success
	@Ignore
	@Test
	public void userApiTest() {
		UserRequestDto requestDto=new UserRequestDto();
		requestDto.setProviderCallbackHost("abc.com");
		
		given().body(requestDto).contentType(ContentType.JSON).when()
		.post("http://localhost:8080/MFSMtnMoMoWeb/userapi").then()
		.statusCode(equalTo(HttpStatus.OK.value())).body("message", is("User Created"));
}
	
	//Valid
	
	@Ignore
		@Test
		public void userApiNotNullTest() {
			UserRequestDto requestDto=new UserRequestDto();
			requestDto.setProviderCallbackHost(null);
			
			given().body(requestDto).contentType(ContentType.JSON).when()
			.post("http://localhost:8080/MFSMtnMoMoWeb/userapi").then()
			.statusCode(equalTo(HttpStatus.OK.value())).body("code", is("ER206"));
	}
}
