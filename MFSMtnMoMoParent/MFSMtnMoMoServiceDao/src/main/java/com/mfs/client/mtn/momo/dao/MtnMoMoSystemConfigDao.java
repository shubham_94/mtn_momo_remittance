package com.mfs.client.mtn.momo.dao;

import java.util.Map;

import com.mfs.client.mtn.momo.exception.DaoException;

public interface MtnMoMoSystemConfigDao {
	
	public Map<String, String> getConfigDetailsMap() throws DaoException;

}
