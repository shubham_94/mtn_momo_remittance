package com.mfs.client.mtn.momo.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.mtn.momo.dao.MtnMoMoSystemConfigDao;
import com.mfs.client.mtn.momo.dao.TransactionDao;
import com.mfs.client.mtn.momo.dto.BalanceResponseDto;
import com.mfs.client.mtn.momo.dto.TokenResponseDto;
import com.mfs.client.mtn.momo.exception.DaoException;
import com.mfs.client.mtn.momo.models.AuthDetailsModel;
import com.mfs.client.mtn.momo.models.AuthorizationModel;
import com.mfs.client.mtn.momo.models.BalanceEnquiryModel;
import com.mfs.client.mtn.momo.models.MtnMomoEventLog;
import com.mfs.client.mtn.momo.service.AuthorizationTokenService;
import com.mfs.client.mtn.momo.service.BalanceEnquiryService;
import com.mfs.client.mtn.momo.util.CommonConstant;
import com.mfs.client.mtn.momo.util.HttpsConnectionRequest;
import com.mfs.client.mtn.momo.util.HttpsConnectionResponse;
import com.mfs.client.mtn.momo.util.HttpsConnectorImpl;
import com.mfs.client.mtn.momo.util.JSONToObjectConversion;
import com.mfs.client.mtn.momo.util.MFSMtnMomoConstant;
import com.mfs.client.mtn.momo.util.MomoCodes;
import com.mfs.client.mtn.momo.util.TokenExpirationCheck;

@Service("BalanceEnquiryService")
public class BalanceEnquiryServiceImpl implements BalanceEnquiryService {

	private static final Logger LOGGER = Logger.getLogger(BalanceEnquiryServiceImpl.class);

	@Autowired
	MtnMoMoSystemConfigDao mtnMoMoSystemConfigDao;

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	TokenExpirationCheck tokenExpirationCheck;

	@Autowired
	AuthorizationTokenService authService;

	@Resource(name = "responseC")
	private Properties responseCodes;

	public BalanceResponseDto balanceService(String countryCode) {

		BalanceResponseDto balanceResponseDto = new BalanceResponseDto();
		AuthorizationModel authModel = null;
		HttpsConnectionResponse httpsConResult = null;
		Map<String, String> systemConfigDetails = null;
		String str_result = null;
		String request = null;
		TokenResponseDto authResponse = null;
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		Map<String, String> headerMap = new HashMap<String, String>();
		HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
		AuthDetailsModel authDetails = null;

		try {

			// get access token
			authModel = transactionDao.getAuthtokenByCountryCode(countryCode);

			// check null token
			if (authModel == null) {
				authResponse = authService.createToken(countryCode);
				if (authResponse.getMessage() != null) {
					balanceResponseDto.setCode(authResponse.getCode());
					balanceResponseDto.setMessage(authResponse.getMessage());
					return balanceResponseDto;
				}
				authModel = transactionDao.getAuthtokenByCountryCode(countryCode);
			}

			int tokenExpiredTime = CommonConstant.expirationTime;
			int tokenTime = tokenExpirationCheck.invalidSession(authModel);

			// check whether token expire or not
			if (tokenTime >= tokenExpiredTime) {

				authResponse = authService.createToken(countryCode);
				if (authResponse.getMessage() != null) {
					balanceResponseDto.setCode(authResponse.getCode());
					balanceResponseDto.setMessage(authResponse.getMessage());
					return balanceResponseDto;
				}
				authModel = transactionDao.getAuthtokenByCountryCode(countryCode);
			}

			// get config details
			systemConfigDetails = mtnMoMoSystemConfigDao.getConfigDetailsMap();

			// check null config details
			if (systemConfigDetails == null) {
				balanceResponseDto.setCode(MFSMtnMomoConstant.ER237);
				balanceResponseDto.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER237));
				return balanceResponseDto;
			}

			connectionRequest.setHttpmethodName("GET");

			// check port null or not
			if (null != systemConfigDetails.get(CommonConstant.PORT)) {
				connectionRequest.setPort(Integer.parseInt(systemConfigDetails.get(CommonConstant.PORT)));
			}

			connectionRequest.setServiceUrl(systemConfigDetails.get(CommonConstant.BASE_URL)
					+ systemConfigDetails.get(CommonConstant.GET_BALANCE));
			// get header values from auth details table
			authDetails = transactionDao.getAuthDetailsByCountryCode(countryCode);

			if (null == authDetails) {
				balanceResponseDto.setCode(MFSMtnMomoConstant.ER242);
				balanceResponseDto.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER242));
				return balanceResponseDto;
			}

			headerMap.put(CommonConstant.SUBSCRIPTION_KEY, authDetails.getSubscriptionKey());

			headerMap.put(CommonConstant.TARGET_ENVIRONMENT, authDetails.getxTargetEnvironment());

			headerMap.put(CommonConstant.AUTHORIZATION, CommonConstant.BEARER + authModel.getAccessToken());

			connectionRequest.setHeaders(headerMap);

			LOGGER.info("BalanceEnquiryServiceImpl in BalanceEnquiryService function balance partner request");

			httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(request, connectionRequest);

			LOGGER.info("BalanceEnquiryServiceImpl in BalanceEnquiryService function balance partner response : "
					+ httpsConResult);

			// check null result
			if (httpsConResult == null) {
				balanceResponseDto.setCode(MFSMtnMomoConstant.ER213);
				balanceResponseDto.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER213));
				return balanceResponseDto;
			}
			str_result = httpsConResult.getResponseData();

			// log event response
			eventResponseLog(str_result);

			if (null != httpsConResult.getTxMessage()
					&& httpsConResult.getTxMessage().equals(MomoCodes.ER401.getMessage())) {

				authResponse = authService.createToken(countryCode);
				if (authResponse.getMessage() != null) {
					balanceResponseDto.setCode(authResponse.getCode());
					balanceResponseDto.setMessage(authResponse.getMessage());
					return balanceResponseDto;
				}

				// set new token
				headerMap.put(CommonConstant.AUTHORIZATION, CommonConstant.BEARER + authResponse.getAccess_token());

				connectionRequest.setHeaders(headerMap);

				LOGGER.info("BalanceEnquiryServiceImpl in BalanceEnquiryService function balance partner request");
				httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(request, connectionRequest);

				LOGGER.info("BalanceEnquiryServiceImpl in BalanceEnquiryService function balance partner response : "
						+ httpsConResult);

				// check null result
				if (httpsConResult == null) {
					balanceResponseDto.setCode(MFSMtnMomoConstant.ER213);
					balanceResponseDto.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER213));
					return balanceResponseDto;
				}
				str_result = httpsConResult.getResponseData();

				// log event response
				eventResponseLog(str_result);
			}

			// handle success and fail response
			if (httpsConResult.getRespCode() == MomoCodes.S200.getCode()) {

				balanceResponseDto = (BalanceResponseDto) JSONToObjectConversion.getObjectFromJson(str_result,
						BalanceResponseDto.class);

				logBalance(balanceResponseDto);

			} else {
				balanceResponseDto.setCode(httpsConResult.getCode());
				balanceResponseDto.setMessage(httpsConResult.getTxMessage());
			}
		} catch (DaoException de) {
			LOGGER.error("==>DaoException in BalanceEnquiryServiceImpl", de);
			balanceResponseDto.setCode(de.getStatus().getStatusCode());
			balanceResponseDto.setMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>Exception in BalanceEnquiryServiceImpl", e);
			balanceResponseDto.setCode(MFSMtnMomoConstant.ER203);
			balanceResponseDto.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER203));
		}
		return balanceResponseDto;
	}

	private void eventResponseLog(String response) throws DaoException {
		MtnMomoEventLog eventRequestLog = new MtnMomoEventLog();
		eventRequestLog.setResponse(response);
		eventRequestLog.setServiceName(CommonConstant.BALANCE_ENQUIRY_SERVICE);
		eventRequestLog.setDateLogged(new Date());
		transactionDao.save(eventRequestLog);
	}

	// log get balance response
	private void logBalance(BalanceResponseDto balanceResponseDto) throws DaoException {
		BalanceEnquiryModel balance = new BalanceEnquiryModel();
		balance.setAvailableBalance(balanceResponseDto.getAvailableBalance());
		balance.setCurrency(balanceResponseDto.getCurrency());
		balance.setDateLogged(new Date());
		transactionDao.save(balance);
	}

}
