package com.mfs.client.mtn.momo.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.mtn.momo.dao.MtnMoMoSystemConfigDao;
import com.mfs.client.mtn.momo.dao.TransactionDao;
import com.mfs.client.mtn.momo.dto.TokenResponseDto;
import com.mfs.client.mtn.momo.dto.ValidateAccountResponseDto;
import com.mfs.client.mtn.momo.exception.DaoException;
import com.mfs.client.mtn.momo.models.AuthDetailsModel;
import com.mfs.client.mtn.momo.models.AuthorizationModel;
import com.mfs.client.mtn.momo.models.MtnMomoEventLog;
import com.mfs.client.mtn.momo.models.ValidateAccount;
import com.mfs.client.mtn.momo.service.AccountHolderService;
import com.mfs.client.mtn.momo.service.AuthorizationTokenService;
import com.mfs.client.mtn.momo.util.CommonConstant;
import com.mfs.client.mtn.momo.util.HttpsConnectionRequest;
import com.mfs.client.mtn.momo.util.HttpsConnectionResponse;
import com.mfs.client.mtn.momo.util.HttpsConnectorImpl;
import com.mfs.client.mtn.momo.util.JSONToObjectConversion;
import com.mfs.client.mtn.momo.util.MFSMtnMomoConstant;
import com.mfs.client.mtn.momo.util.MomoCodes;
import com.mfs.client.mtn.momo.util.TokenExpirationCheck;

@Service("AccountHolderService")
public class AccountHolderServiceImpl implements AccountHolderService {

	@Autowired
	MtnMoMoSystemConfigDao mtnMoMoSystemConfigDao;

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	TokenExpirationCheck tokenExpirationCheck;

	@Autowired
	AuthorizationTokenService authService;

	@Resource(name = "responseC")
	private Properties responseCodes;

	private static final Logger LOGGER = Logger.getLogger(AccountHolderServiceImpl.class);

	public ValidateAccountResponseDto getAccountHolderService(String msisdn, String countryCode) {

		ValidateAccountResponseDto response = new ValidateAccountResponseDto();
		HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
		Map<String, String> configMap = null;
		Map<String, String> headerMap = new HashMap<String, String>();
		HttpsConnectionResponse httpsConResult = null;
		String str_result = null;
		AuthorizationModel authModel = null;
		TokenResponseDto authResponse = new TokenResponseDto();
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		AuthDetailsModel authDetails = null;

		try {

			// get access token
			authModel = transactionDao.getAuthtokenByCountryCode(countryCode);

			// check null token
			if (authModel == null) {
				authResponse = authService.createToken(countryCode);
				if (authResponse.getMessage() != null) {
					response.setCode(authResponse.getCode());
					response.setMessage(authResponse.getMessage());
					return response;
				}
				authModel = transactionDao.getAuthtokenByCountryCode(countryCode);
			}

			int tokenExpiredTime = CommonConstant.expirationTime;
			int tokenTime = tokenExpirationCheck.invalidSession(authModel);

			// check whether token expire or not
			if (tokenTime >= tokenExpiredTime) {

				authResponse = authService.createToken(countryCode);
				if (authResponse.getMessage() != null) {
					response.setCode(authResponse.getCode());
					response.setMessage(authResponse.getMessage());
					return response;
				}
				authModel = transactionDao.getAuthtokenByCountryCode(countryCode);
			}

			// get config details
			configMap = mtnMoMoSystemConfigDao.getConfigDetailsMap();

			// check null config details
			if (configMap == null) {
				response.setCode(MFSMtnMomoConstant.ER237);
				response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER237));
				return response;
			}

			connectionRequest.setHttpmethodName("GET");

			// check port null or not
			if (null != configMap.get(CommonConstant.PORT)) {
				connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)));
			}

			authDetails = transactionDao.getAuthDetailsByCountryCode(countryCode);

			if (null == authDetails) {
				response.setCode(MFSMtnMomoConstant.ER242);
				response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER242));
				return response;
			}

			headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);

			headerMap.put(CommonConstant.SUBSCRIPTION_KEY, authDetails.getSubscriptionKey());

			headerMap.put(CommonConstant.AUTHORIZATION, CommonConstant.BEARER + authModel.getAccessToken());

			headerMap.put(CommonConstant.TARGET_ENVIRONMENT, authDetails.getxTargetEnvironment());

			connectionRequest.setHeaders(headerMap);

			String url = (configMap.get(CommonConstant.BASE_URL) + configMap.get(CommonConstant.ACCOUNTHOLDER) + msisdn
					+ configMap.get(CommonConstant.BASIC_USER_INFO));
			connectionRequest.setServiceUrl(url);

			// log event request
			eventRequestLog(msisdn);

			LOGGER.info("AccountHolderServiceImpl in getAccountHolderService function partner request" + url);

			httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(null, connectionRequest);

			LOGGER.info(
					"AccountHolderServiceImpl in getAccountHolderService function partner response" + httpsConResult);

			if (httpsConResult == null) {
				response.setCode(MFSMtnMomoConstant.ER213);
				response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER213));
				return response;
			}

			str_result = httpsConResult.getResponseData();

			// Event response log
			eventResponseLog(str_result);

			if (null != httpsConResult.getTxMessage()
					&& httpsConResult.getTxMessage().equals(MomoCodes.ER401.getMessage())) {

				authResponse = authService.createToken(countryCode);
				if (authResponse.getMessage() != null) {
					response.setCode(authResponse.getCode());
					response.setMessage(authResponse.getMessage());
					return response;
				}

				// set new token
				headerMap.put(CommonConstant.AUTHORIZATION, CommonConstant.BEARER + authResponse.getAccess_token());
				connectionRequest.setHeaders(headerMap);

				LOGGER.info("AccountHolderServiceImpl in getAccountHolderService function partner request" + url);
				httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(null, connectionRequest);

				LOGGER.info("AccountHolderServiceImpl in getAccountHolderService function partner response"
						+ httpsConResult);

				// check null result
				if (httpsConResult == null) {
					response.setCode(MFSMtnMomoConstant.ER213);
					response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER213));
					return response;
				}
				str_result = httpsConResult.getResponseData();

				// Event response log
				eventResponseLog(str_result);
			}

			// check success and fail response
			if (httpsConResult.getRespCode() == MomoCodes.S200.getCode()) {

				response = (ValidateAccountResponseDto) JSONToObjectConversion.getObjectFromJson(str_result,
						ValidateAccountResponseDto.class);

				logResponse(response, msisdn);

			} else {
				response = new ValidateAccountResponseDto();
				response.setCode(httpsConResult.getCode());
				response.setMessage(httpsConResult.getTxMessage());
			}
		} catch (DaoException de) {
			LOGGER.error("==>DaoException in getAccountHolderService", de);
			response.setCode(de.getStatus().getStatusCode());
			response.setMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>Exception in getAccountHolderService", e);
			response.setCode(MFSMtnMomoConstant.ER203);
			response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER203));
		}
		return response;
	}

	private void eventResponseLog(String response) throws DaoException {
		MtnMomoEventLog transEventLog = transactionDao.getByLastRecord();
		if (transEventLog != null) {
			transEventLog.setResponse(response);
			transEventLog.setDateLogged(new Date());
			transactionDao.update(transEventLog);
		}
	}

	private void eventRequestLog(String request) throws DaoException {
		MtnMomoEventLog eventRequestLog = new MtnMomoEventLog();
		eventRequestLog.setMfsId(null);
		eventRequestLog.setRequest(request);
		eventRequestLog.setServiceName(CommonConstant.ACCOUNT_HOLDER_SERVICE);
		eventRequestLog.setDateLogged(new Date());
		transactionDao.save(eventRequestLog);
	}

	private void logResponse(ValidateAccountResponseDto response, String msisdn) throws DaoException {
		ValidateAccount accountDetails = transactionDao.getAccountDetailsByMsisdn(msisdn);
		if (null != accountDetails) {
			accountDetails.setSub(response.getSub());
			accountDetails.setName(response.getName());
			accountDetails.setGivenName(response.getGiven_name());
			accountDetails.setFamilyName(response.getFamily_name());
			accountDetails.setBirthdate(response.getBirthdate());
			accountDetails.setLocale(response.getLocale());
			accountDetails.setGender(response.getGender());
			accountDetails.setUpdatedAt(response.getUpdated_at());
			accountDetails.setDateLogged(new Date());
			transactionDao.update(accountDetails);
		} else {
			ValidateAccount account = new ValidateAccount();
			account.setAccountHolderId(msisdn);
			account.setSub(response.getSub());
			account.setName(response.getName());
			account.setGivenName(response.getGiven_name());
			account.setFamilyName(response.getFamily_name());
			account.setBirthdate(response.getBirthdate());
			account.setLocale(response.getLocale());
			account.setGender(response.getGender());
			account.setUpdatedAt(response.getUpdated_at());
			account.setDateLogged(new Date());
			transactionDao.save(account);
		}
	}
}