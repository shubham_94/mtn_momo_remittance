package com.mfs.client.mtn.momo.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "momo_transaction_error")
public class TransactionErrorModel {

	@Id
	@GeneratedValue
	@Column(name = "transaction_error_id")
	private int transactionErrorId;

	@Column(name = "amount")
	private String amount;

	@Column(name = "currency")
	private String currency;

	@Column(name = "external_id")
	private String externalId;

	@Column(name = "payer_party_id_type")
	private String payerPartyIdType;

	@Column(name = "payer_party_id")
	private String payerPartyId;

	@Column(name = "payer_message")
	private String payerMessage;

	@Column(name = "payee_note")
	private String payeeNote;

	@Column(name = "txn_error_code")
	private String txnErrorCode;

	@Column(name = "txn_error_message")
	private String txnErrorMessage;

	@Column(name = "date")
	private Date date;

	public int getTransactionErrorId() {
		return transactionErrorId;
	}

	public void setTransactionErrorId(int transactionErrorId) {
		this.transactionErrorId = transactionErrorId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getPayerPartyIdType() {
		return payerPartyIdType;
	}

	public void setPayerPartyIdType(String payerPartyIdType) {
		this.payerPartyIdType = payerPartyIdType;
	}

	public String getPayerPartyId() {
		return payerPartyId;
	}

	public void setPayerPartyId(String payerPartyId) {
		this.payerPartyId = payerPartyId;
	}

	public String getPayerMessage() {
		return payerMessage;
	}

	public void setPayerMessage(String payerMessage) {
		this.payerMessage = payerMessage;
	}

	public String getPayeeNote() {
		return payeeNote;
	}

	public void setPayeeNote(String payeeNote) {
		this.payeeNote = payeeNote;
	}

	public String getTxnErrorCode() {
		return txnErrorCode;
	}

	public void setTxnErrorCode(String txnErrorCode) {
		this.txnErrorCode = txnErrorCode;
	}

	public String getTxnErrorMessage() {
		return txnErrorMessage;
	}

	public void setTxnErrorMessage(String txnErrorMessage) {
		this.txnErrorMessage = txnErrorMessage;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "TransactionErrorModel [transactionErrorId=" + transactionErrorId + ", amount=" + amount + ", currency="
				+ currency + ", externalId=" + externalId + ", payerPartyIdType=" + payerPartyIdType + ", payerPartyId="
				+ payerPartyId + ", payerMessage=" + payerMessage + ", payeeNote=" + payeeNote + ", txnErrorCode="
				+ txnErrorCode + ", txnErrorMessage=" + txnErrorMessage + ", date=" + date + "]";
	}

}
