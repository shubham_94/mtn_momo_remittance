package com.mfs.client.mtn.momo.dao;

import com.mfs.client.mtn.momo.exception.DaoException;

public interface BaseDao {
	
	public boolean save(Object obj) throws DaoException;

	public boolean update(Object obj) throws DaoException;

	public boolean saveOrUpdate(Object obj) throws DaoException;

	public boolean delete(Object obj) throws DaoException;

	
}
