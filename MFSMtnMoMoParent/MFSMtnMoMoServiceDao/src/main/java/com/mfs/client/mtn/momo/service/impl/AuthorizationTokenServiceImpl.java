package com.mfs.client.mtn.momo.service.impl;

import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.mtn.momo.dao.MtnMoMoSystemConfigDao;
import com.mfs.client.mtn.momo.dao.TransactionDao;
import com.mfs.client.mtn.momo.dto.TokenResponseDto;
import com.mfs.client.mtn.momo.exception.DaoException;
import com.mfs.client.mtn.momo.models.AuthDetailsModel;
import com.mfs.client.mtn.momo.models.AuthorizationModel;
import com.mfs.client.mtn.momo.models.MtnMomoEventLog;
import com.mfs.client.mtn.momo.service.AuthorizationTokenService;
import com.mfs.client.mtn.momo.util.CommonConstant;
import com.mfs.client.mtn.momo.util.HttpsConnectionRequest;
import com.mfs.client.mtn.momo.util.HttpsConnectionResponse;
import com.mfs.client.mtn.momo.util.HttpsConnectorImpl;
import com.mfs.client.mtn.momo.util.JSONToObjectConversion;
import com.mfs.client.mtn.momo.util.MFSMtnMomoConstant;
import com.mfs.client.mtn.momo.util.MomoCodes;

@Service("AuthorizationTokenService")
public class AuthorizationTokenServiceImpl implements AuthorizationTokenService {

	private static final Logger LOGGER = Logger.getLogger(AuthorizationTokenServiceImpl.class);

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	MtnMoMoSystemConfigDao moMoSystemConfigDao;

	@Resource(name = "responseC")
	private Properties responseCodes;

	public TokenResponseDto createToken(String countryCode) {

		TokenResponseDto response = new TokenResponseDto();
		Map<String, String> systemConfigDetails = null;
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		HttpsConnectionResponse httpsConResult = null;
		Map<String, String> headerMap = new HashMap<String, String>();
		String str_result = null;
		StringBuilder string = new StringBuilder();
		HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
		String connectionData = "";
		AuthDetailsModel authDetails = null;

		try {
			authDetails = transactionDao.getAuthDetailsByCountryCode(countryCode);

			if (null == authDetails) {
				response.setCode(MFSMtnMomoConstant.ER242);
				response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER242));
				return response;
			}

			// check wheather api key and reference id is null or not for given country code
			if (authDetails.getApiKey() == null || authDetails.getApiUserId() == null) {
				response.setCode(MFSMtnMomoConstant.ER218);
				response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER218));
			} else {
				// get system config details
				systemConfigDetails = moMoSystemConfigDao.getConfigDetailsMap();

				// check null config details
				if (systemConfigDetails == null) {
					response.setCode(MFSMtnMomoConstant.ER237);
					response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER237));
					return response;
				}

				string.append(authDetails.getApiUserId());
				string.append(":");
				string.append(authDetails.getApiKey());

				String basicAuth = "Basic " + new String(Base64.getEncoder().encode(string.toString().getBytes()));
				headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
				headerMap.put(CommonConstant.AUTHORIZATION, basicAuth);
				headerMap.put(CommonConstant.SUBSCRIPTION_KEY, authDetails.getSubscriptionKey());
				connectionRequest.setHeaders(headerMap);
				connectionRequest.setHttpmethodName("POST");

				// check port null or not
				if (null != systemConfigDetails.get(CommonConstant.PORT)) {
					connectionRequest.setPort(Integer.parseInt(systemConfigDetails.get(CommonConstant.PORT)));
				}

				String url = systemConfigDetails.get(CommonConstant.BASE_URL)
						+ systemConfigDetails.get(CommonConstant.TOKEN_URL);

				connectionRequest.setServiceUrl(url);

				// log event request
				eventRequestLog(basicAuth);

				LOGGER.info("AuthorizationTokenServiceImpl in createToken function partner request basicAuth : "
						+ basicAuth + " subscription_key :" + authDetails.getSubscriptionKey() + " URL: " + url);

				httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(connectionData, connectionRequest);

				LOGGER.info(
						"AuthorizationTokenServiceImpl in createToken function partner response : " + httpsConResult);

				// check null result
				if (httpsConResult == null) {
					response.setCode(MFSMtnMomoConstant.ER213);
					response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER213));
					return response;
				}

				str_result = httpsConResult.getResponseData();

				// event response log
				eventResponseLog(str_result);

				// check success and fail response
				if (httpsConResult.getRespCode() == MomoCodes.S200.getCode()) {

					response = (TokenResponseDto) JSONToObjectConversion.getObjectFromJson(str_result,
							TokenResponseDto.class);

					logResponse(response, countryCode);

				} else {
					response.setCode(httpsConResult.getCode());
					response.setMessage(httpsConResult.getTxMessage());
				}
			}
		} catch (DaoException de) {
			LOGGER.error("==>DaoException in AuthorizationTokenServiceImpl", de);
			response.setCode(de.getStatus().getStatusCode());
			response.setMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>Exception in AuthorizationTokenServiceImpl", e);
			response.setCode(MFSMtnMomoConstant.ER203);
			response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER203));
		}
		return response;
	}

	private void eventResponseLog(String response) throws DaoException {
		MtnMomoEventLog transEventLog = transactionDao.getByLastRecord();
		if (transEventLog != null) {
			transEventLog.setResponse(response);
			transEventLog.setDateLogged(new Date());
			transactionDao.update(transEventLog);
		}
	}

	private void eventRequestLog(String request) throws DaoException {
		MtnMomoEventLog eventRequestLog = new MtnMomoEventLog();
		eventRequestLog.setMfsId(null);
		eventRequestLog.setRequest(request);
		eventRequestLog.setServiceName(CommonConstant.AUTHORIZATION_TOKEN_SERVICE);
		eventRequestLog.setDateLogged(new Date());
		transactionDao.save(eventRequestLog);
	}

	private void logResponse(TokenResponseDto response, String countryCode) throws DaoException {
		AuthorizationModel authlog = null;
		authlog = transactionDao.getAuthtokenByCountryCode(countryCode);
		if (authlog != null) {
			authlog.setAccessToken(response.getAccess_token());
			authlog.setTokenType(response.getToken_type());
			authlog.setExpiresIn(response.getExpires_in());
			authlog.setDateLogged(new Date());
			transactionDao.update(authlog);
		} else {
			AuthorizationModel auth = new AuthorizationModel();
			auth.setAccessToken(response.getAccess_token());
			auth.setTokenType(response.getToken_type());
			auth.setExpiresIn(response.getExpires_in());
			auth.setCountryCode(countryCode);
			auth.setDateLogged(new Date());
			transactionDao.save(auth);
		}
	}
}