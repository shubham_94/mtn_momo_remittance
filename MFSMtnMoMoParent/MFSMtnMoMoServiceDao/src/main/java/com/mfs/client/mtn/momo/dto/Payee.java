package com.mfs.client.mtn.momo.dto;

public class Payee {

	private String partyIdType;
	private String partyId;

	public String getPartyIdType() {
		return partyIdType;
	}

	public void setPartyIdType(String partyIdType) {
		this.partyIdType = partyIdType;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	@Override
	public String toString() {
		return "Payee [partyIdType=" + partyIdType + ", partyId=" + partyId + "]";
	}

}
