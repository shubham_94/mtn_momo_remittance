package com.mfs.client.mtn.momo.service;

import com.mfs.client.mtn.momo.dto.GetUserResponseDto;

public interface GetUserService {

	GetUserResponseDto getUserService();
}
