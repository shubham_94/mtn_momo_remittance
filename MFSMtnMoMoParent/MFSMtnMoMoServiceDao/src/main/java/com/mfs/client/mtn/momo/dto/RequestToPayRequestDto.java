package com.mfs.client.mtn.momo.dto;

public class RequestToPayRequestDto {
	private String amount;
	private String currency;
	private String externalId;
	private Payee payee;
	private String payerMessage;
	private String payeeNote;
	private String countryCode;

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public Payee getPayee() {
		return payee;
	}

	public void setPayee(Payee payee) {
		this.payee = payee;
	}

	public String getPayerMessage() {
		return payerMessage;
	}

	public void setPayerMessage(String payerMessage) {
		this.payerMessage = payerMessage;
	}

	public String getPayeeNote() {
		return payeeNote;
	}

	public void setPayeeNote(String payeeNote) {
		this.payeeNote = payeeNote;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	@Override
	public String toString() {
		return "RequestToPayRequestDto [amount=" + amount + ", currency=" + currency + ", externalId=" + externalId
				+ ", payee=" + payee + ", payerMessage=" + payerMessage + ", payeeNote=" + payeeNote + ", countryCode="
				+ countryCode + "]";
	}

}