package com.mfs.client.mtn.momo.dto;

public class UserRequestDto {
	

	private String providerCallbackHost;

	
	public String getProviderCallbackHost() {
		return providerCallbackHost;
	}

	public void setProviderCallbackHost(String providerCallbackHost) {
		this.providerCallbackHost = providerCallbackHost;
	}

	@Override
	public String toString() {
		return "UserRequestDto [providerCallbackHost=" + providerCallbackHost + "]";
	}

	
	
}