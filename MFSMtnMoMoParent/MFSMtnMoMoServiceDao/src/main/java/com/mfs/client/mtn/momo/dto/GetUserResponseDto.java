package com.mfs.client.mtn.momo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)

public class GetUserResponseDto {

	private String providerCallbackHost;
	private String targetEnvironment;
	private String code;
	private String message;

	public String getProviderCallbackHost() {
		return providerCallbackHost;
	}

	public void setProviderCallbackHost(String providerCallbackHost) {
		this.providerCallbackHost = providerCallbackHost;
	}

	public String getTargetEnvironment() {
		return targetEnvironment;
	}

	public void setTargetEnvironment(String targetEnvironment) {
		this.targetEnvironment = targetEnvironment;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "GetUserResponseDto [providerCallbackHost=" + providerCallbackHost + ", targetEnvironment="
				+ targetEnvironment + ", code=" + code + ", message=" + message + "]";
	}

}
