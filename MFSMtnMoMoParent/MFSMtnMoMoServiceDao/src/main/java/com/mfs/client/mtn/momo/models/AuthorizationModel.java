package com.mfs.client.mtn.momo.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "momo_authorization_token")
public class AuthorizationModel {

	@Id
	@GeneratedValue
	@Column(name = "auth_id")
	private int authId;

	@Column(name = "access_token", length = 600)
	private String accessToken;

	@Column(name = "token_type")
	private String tokenType;

	@Column(name = "expires_in")
	private String expiresIn;

	@Column(name = "country_code")
	private String countryCode;

	@Column(name = "date_logged")
	private Date dateLogged;

	public int getAuthId() {
		return authId;
	}

	public void setAuthId(int authId) {
		this.authId = authId;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public String getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(String expiresIn) {
		this.expiresIn = expiresIn;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	@Override
	public String toString() {
		return "AuthorizationModel [authId=" + authId + ", accessToken=" + accessToken + ", tokenType=" + tokenType
				+ ", expiresIn=" + expiresIn + ", countryCode=" + countryCode + ", dateLogged=" + dateLogged + "]";
	}

}
