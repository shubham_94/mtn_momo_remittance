package com.mfs.client.mtn.momo.util;

public class CommonConstant {

	public static final String BASE_URL = "base_url";
	public static final String CONTENT_TYPE = "Content-type";
	public static final String APPLICATION_JSON = "application/json";

	public static final String PORT = "port";
	public static final String HTTP_METHOD = "httpmethod";
	public static final String ISHTTPS = "ishttps";
	public static final String USER_URL = "user-url";
	public static final String SUBSCRIPTION_KEY = "Ocp-Apim-Subscription-Key";
	public static final String REFERENCE_ID = "X-Reference-Id";
	public static final String AUTHORIZATION = "Authorization";
	public static final String TARGET_ENVIRONMENT = "X-Target-Environment";
	public static final String CALLBACK_URL = "X-Callback-Url";
	public static final String CODE202 = "202";
	public static final String INVALID_AMOUNT = "invalid amount";
	public static final String INVALID_CURRENCY = "invalid currency";
	public static final String INVALID_EXTERNAL_ID = "invalid external id";
	public static final String INVALID_PARTY_ID_TYPE = "invalid party id type";
	public static final String INVALID_PARTY_ID = "invalid party id";
	public static final String INVALID_PAYER_MESSAGE = "invalid payer message";
	public static final String INVALID_PAYEE_NOTE = "invalid payer note";
	public static final String TRANSFER_URL = "transfer_url";
	public static final String GET_REQUEST_TO_PAY_URL = "get_request_to_pay_url";

	public static final String COLLECTION = "collection";
	public static final String ACCOUNTHOLDER = "account_holder_url";

	public static final String ACCOUNTHOLDERTYPE = "accountHolder-type";
	public static final String ACCOUNTHOLDERBASEURL = "accountHolder_base_url";
	public static final String ACTIVE = "active";

	public static final String API_KEY_URL = "api-key-url";
	public static final String TOKEN_URL = "token-url";

	public static final String GET_BALANCE = "get_balance_url";
	public static final String BASIC = "Basic ";
	public static final String SUBSCRIPTIONKEY = "subscription_key";
	public static final String BEARER = "Bearer ";
	public static final String INVALID_PROVIDER_CALLBACK_HOST = "invalid provider callback host";
	public static final String HTTP_METHOD_GET = "http_method_get";
	public static final String MSISDN_VALIDATION = "Please prov	ide valid msisdn value";
	public static final String EMAIL_VALIDATION = "Please provide valid email value";
	public static final String PARTY_CODE_VALIDATION = "Please provide valid party code value";
	public static final String ACCOUNT_HOLDER_TYPE_VALIDATION = "Please provide valid account holder type in lowercase";
	public static final String PARTY_ID_TYPE = "party_id_type";
	public static final String ACCOUNT_HOLDER_SERVICE = "Account Holder Service";
	public static final String API_KEY_SERVICE = "Api Key Service";
	public static final String GET_REQUEST_TO_PAY_SERVICE = "Get Request To Pay Service";
	public static final String GET_USER_SERVICE = "Get User service";
	public static final String USER_SERVICE = "User Service";
	public static final String REQUEST_TO_PAY_SERVICE = "Request To Pay Service";
	public static final String BALANCE_ENQUIRY_SERVICE = "Balance Enquiry Service";
	public static final String AUTHORIZATION_TOKEN_SERVICE = "Authorization Token Service";
	public static final String PARTNER_SYSTEM_ERROR = "Partner System Error";
	public static final String USER_NAME = "Username";
	public static final String USER_NAME_VALUE = "username";
	public static final String PASSWORD = "Password";
	public static final String PASSWORD_VALUE = "password";
	public static final String TARGET_ENV_VALUE = "X-Target-Environment";
	
	public static final String BASIC_USER_INFO = "account_holder_basicuserinfo";
	public static final String STATUS_URL = "status_url";
	public static final String SANDBOX_PROVISIONING_URL = "sandbox_provisioning_base_url";



	public static int expirationTime =59;

}
