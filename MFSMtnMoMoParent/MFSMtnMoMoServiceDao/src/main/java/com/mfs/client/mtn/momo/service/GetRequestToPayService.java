package com.mfs.client.mtn.momo.service;

import com.mfs.client.mtn.momo.dto.GetRequestToPayResponseDto;

public interface GetRequestToPayService {

	public GetRequestToPayResponseDto getRequestToPay(String externalId);
}
