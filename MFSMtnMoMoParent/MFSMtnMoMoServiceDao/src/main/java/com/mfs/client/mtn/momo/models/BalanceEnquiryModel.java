package com.mfs.client.mtn.momo.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "momo_get_balance")
public class BalanceEnquiryModel {

	@Id
	@GeneratedValue
	private int balanceId;

	@Column(name = "available_balance")
	private String availableBalance;

	@Column(name = "currency")
	private String currency;

	@Column(name = "date_logged")
	private Date dateLogged;

	public String getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(String availableBalance) {
		this.availableBalance = availableBalance;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public int getBalanceId() {
		return balanceId;
	}

	public void setBalanceId(int balanceId) {
		this.balanceId = balanceId;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	@Override
	public String toString() {
		return "BalanceEnquiryModel [balanceId=" + balanceId + ", availableBalance=" + availableBalance + ", currency="
				+ currency + ", dateLogged=" + dateLogged + "]";
	}

}