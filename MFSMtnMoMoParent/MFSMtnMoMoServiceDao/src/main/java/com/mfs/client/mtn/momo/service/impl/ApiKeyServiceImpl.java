package com.mfs.client.mtn.momo.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.mtn.momo.dao.MtnMoMoSystemConfigDao;
import com.mfs.client.mtn.momo.dao.TransactionDao;
import com.mfs.client.mtn.momo.dto.ApiKeyResponseDto;
import com.mfs.client.mtn.momo.exception.DaoException;
import com.mfs.client.mtn.momo.models.MtnMomoEventLog;
import com.mfs.client.mtn.momo.models.UserModel;
import com.mfs.client.mtn.momo.service.ApiKeyService;
import com.mfs.client.mtn.momo.util.CommonConstant;
import com.mfs.client.mtn.momo.util.HttpsConnectionRequest;
import com.mfs.client.mtn.momo.util.HttpsConnectionResponse;
import com.mfs.client.mtn.momo.util.HttpsConnectorImpl;
import com.mfs.client.mtn.momo.util.JSONToObjectConversion;
import com.mfs.client.mtn.momo.util.MFSMtnMomoConstant;
import com.mfs.client.mtn.momo.util.MomoCodes;

@Service("ApiKeyService")
public class ApiKeyServiceImpl implements ApiKeyService {

	private static final Logger LOGGER = Logger.getLogger(ApiKeyServiceImpl.class);

	@Autowired
	MtnMoMoSystemConfigDao moMoSystemConfigDao;

	@Autowired
	TransactionDao transactionDao;

	@Resource(name = "responseC")
	private Properties responseCodes;

	// Prepare Api Key For MTN Money
	public ApiKeyResponseDto createApiKey() {

		ApiKeyResponseDto response = new ApiKeyResponseDto();
		Map<String, String> systemConfigDetails = null;
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		HttpsConnectionResponse httpsConResult = null;
		Map<String, String> headerMap = new HashMap<String, String>();
		UserModel userModel = null;
		String str_result = null;
		HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();

		try {

			// get user last record
			userModel = transactionDao.getLastRecordByreferenceId();

			// check whether reference id is null or not
			if (userModel == null) {
				response.setStatusCode(MFSMtnMomoConstant.ER220);
				response.setStatusCode(responseCodes.getProperty(MFSMtnMomoConstant.ER220));
			} else {
				// get system config details
				systemConfigDetails = moMoSystemConfigDao.getConfigDetailsMap();

				// check null system config details
				if (systemConfigDetails == null) {
					response.setStatusCode(MFSMtnMomoConstant.ER237);
					response.setStatusCode(responseCodes.getProperty(MFSMtnMomoConstant.ER237));
					return response;
				}

				headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
				headerMap.put(CommonConstant.SUBSCRIPTION_KEY, systemConfigDetails.get(CommonConstant.SUBSCRIPTIONKEY));

				connectionRequest.setHeaders(headerMap);
				connectionRequest.setHttpmethodName("POST");

				// check port null or not
				if (null != systemConfigDetails.get(CommonConstant.PORT)) {
					connectionRequest.setPort(Integer.parseInt(systemConfigDetails.get(CommonConstant.PORT)));
				}

				String url = systemConfigDetails.get(CommonConstant.SANDBOX_PROVISIONING_URL)
						+ systemConfigDetails.get(CommonConstant.USER_URL) + "/" + userModel.getReferenceId()
						+ systemConfigDetails.get(CommonConstant.API_KEY_URL);

				connectionRequest.setServiceUrl(url);

				// event request log
				eventRequestLog(userModel.getReferenceId());

				LOGGER.info("ApiKeyServiceImpl in createApiKey function partner request : " + url);
				httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(url, connectionRequest);
				LOGGER.info("ApiKeyServiceImpl in createApiKey function partner response : " + httpsConResult);

				// check null conresult
				if (httpsConResult == null) {
					response.setStatusCode(MFSMtnMomoConstant.ER213);
					response.setStatusCode(responseCodes.getProperty(MFSMtnMomoConstant.ER213));
					return response;
				}

				str_result = httpsConResult.getResponseData();

				// event response log
				eventResponseLog(str_result);

				// check success and fail response
				if (httpsConResult.getRespCode() == MomoCodes.S201.getCode()) {
					response = (ApiKeyResponseDto) JSONToObjectConversion.getObjectFromJson(str_result,
							ApiKeyResponseDto.class);
					logApiKey(response, userModel);

				} else {
					response = new ApiKeyResponseDto();
					response.setStatusCode(httpsConResult.getCode());
					response.setStatusMessage(httpsConResult.getTxMessage());
				}
			}

			LOGGER.info("ApiKeyServiceImpl in createApiKey function adapter request : " + response);

		} catch (DaoException de) {
			LOGGER.error("==>DaoException in APIKeyServiceImpl", de);
			response = new ApiKeyResponseDto();
			response.setStatusCode(de.getStatus().getStatusCode());
			response.setStatusMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>Exception in APIKeyServiceImpl", e);
			response = new ApiKeyResponseDto();
			response.setStatusCode(MFSMtnMomoConstant.ER203);
			response.setStatusCode(responseCodes.getProperty(MFSMtnMomoConstant.ER203));
		}
		return response;
	}

	private void eventRequestLog(String request) throws DaoException {

		MtnMomoEventLog eventRequestLog = new MtnMomoEventLog();
		eventRequestLog.setMfsId(null);
		eventRequestLog.setRequest(request);
		eventRequestLog.setServiceName(CommonConstant.API_KEY_SERVICE);
		eventRequestLog.setDateLogged(new Date());
		transactionDao.save(eventRequestLog);
	}

	private void eventResponseLog(String response) throws DaoException {

		MtnMomoEventLog transEventLog = transactionDao.getByLastRecord();
		if (transEventLog != null) {
			transEventLog.setResponse(response);
			transEventLog.setDateLogged(new Date());
			transactionDao.update(transEventLog);
		}
	}

	private void logApiKey(ApiKeyResponseDto response, UserModel userModel) throws DaoException {
		userModel.setApiKey(response.getApiKey());
		userModel.setDateLogged(new Date());
		transactionDao.update(userModel);
	}
}