package com.mfs.client.mtn.momo.dao.impl;

import java.util.Properties;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.client.mtn.momo.dao.TransactionDao;
import com.mfs.client.mtn.momo.dto.ResponseStatus;
import com.mfs.client.mtn.momo.exception.DaoException;
import com.mfs.client.mtn.momo.models.AuthDetailsModel;
import com.mfs.client.mtn.momo.models.AuthorizationModel;
import com.mfs.client.mtn.momo.models.MtnMomoEventLog;
import com.mfs.client.mtn.momo.models.TransactionLogModel;
import com.mfs.client.mtn.momo.models.UserModel;
import com.mfs.client.mtn.momo.models.ValidateAccount;
import com.mfs.client.mtn.momo.util.MFSMtnMomoConstant;

@EnableTransactionManagement
@Repository("TransactionDao")
public class TransactionDaoImpl extends BaseDaoImpl implements TransactionDao {

	@Autowired
	SessionFactory sessionFactory;

	@Resource(name = "responseC")
	private Properties responseCodes;

	private static final Logger LOGGER = Logger.getLogger(TransactionDaoImpl.class);

	@Transactional
	public UserModel getTransactionLogByreferenceId(String referenceId) throws DaoException {
		// LOGGER.debug("Inside getTansactionLogByreferenceId of TransactionDaoImpl
		// request:" + referenceId);
		UserModel userModel = null;

		Session session = sessionFactory.getCurrentSession();
		try {
			userModel = (UserModel) session.createQuery("From UserModel where referenceId=:referenceId")
					.setParameter("referenceId", referenceId).uniqueResult();
		} catch (Exception e) {
			LOGGER.error("==>Exception in TransactionDaoImpl ", e);
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(MFSMtnMomoConstant.ER202);
			status.setStatusMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER202));
			throw new DaoException(status);
		}
		return userModel;
	}

	@Transactional
	public TransactionLogModel getTransactionLogByExtrernalId(String externalId) throws DaoException {
		// LOGGER.debug("Inside getTansactionLogByExternalId of TransactionDaoImpl
		// request:" + externalId);
		TransactionLogModel transactionLogModel = null;

		Session session = sessionFactory.getCurrentSession();
		try {
			transactionLogModel = (TransactionLogModel) session
					.createQuery("From TransactionLogModel where externalId=:externalId")
					.setParameter("externalId", externalId).uniqueResult();
		} catch (Exception e) {
			LOGGER.error("==>Exception in TransactionDaoImpl ", e);
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(MFSMtnMomoConstant.ER209);
			status.setStatusMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER209));
			throw new DaoException(status);
		}
		return transactionLogModel;
	}

	@Transactional
	public UserModel getLastRecordByreferenceId() throws DaoException {
		UserModel userModel = null;

		Session session = sessionFactory.getCurrentSession();
		try {
			userModel = (UserModel) session.createQuery("From UserModel order by user_id  DESC").list().get(0);

		} catch (Exception e) {
			LOGGER.error("==>Exception in TransactionDaoImpl ", e);
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(MFSMtnMomoConstant.ER215);
			status.setStatusMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER215));
			throw new DaoException(status);
		}
		return userModel;
	}

	@Transactional
	public AuthorizationModel getLastRecordByAccessToken() throws DaoException {

		AuthorizationModel authorizationModel = null;

		Session session = sessionFactory.getCurrentSession();
		try {
			authorizationModel = (AuthorizationModel) session
					.createQuery("From AuthorizationModel order by auth_id  DESC").list().get(0);
		} catch (Exception e) {
			LOGGER.error("==>Exception in TransactionDaoImpl ", e);
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(MFSMtnMomoConstant.ER215);
			status.setStatusMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER215));
			// throw new DaoException(status);
		}
		return authorizationModel;
	}

	@Transactional
	public TransactionLogModel getTransactionLogByReferenceId(String referenceId) throws DaoException {
		LOGGER.debug("Inside getTansactionLogByExternalId of TransactionDaoImpl request:" + referenceId);
		TransactionLogModel transactionLogModel = null;

		Session session = sessionFactory.getCurrentSession();
		try {
			transactionLogModel = (TransactionLogModel) session
					.createQuery("From TransactionLogModel where referenceId=:referenceId")
					.setParameter("referenceId", referenceId).uniqueResult();
		} catch (Exception e) {
			LOGGER.error("==>Exception in TransactionDaoImpl ", e);
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(MFSMtnMomoConstant.ER209);
			status.setStatusMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER209));
			throw new DaoException(status);
		}
		return transactionLogModel;
	}

	@Transactional
	public TransactionLogModel getTransactionLogByExternalId(String externalId) throws DaoException {
		// LOGGER.debug("Inside getTansactionLogByExternalId of TransactionDaoImpl
		// request:" + externalId);
		TransactionLogModel transactionLogModel = null;

		Session session = sessionFactory.getCurrentSession();
		try {
			transactionLogModel = (TransactionLogModel) session
					.createQuery("From TransactionLogModel where externalId=:externalId")
					.setParameter("externalId", externalId).uniqueResult();
		} catch (Exception e) {
			LOGGER.error("==>Exception in TransactionDaoImpl ", e);
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(MFSMtnMomoConstant.ER209);
			status.setStatusMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER209));
			throw new DaoException(status);
		}
		return transactionLogModel;
	}

	@Transactional
	public MtnMomoEventLog getByLastRecord() throws DaoException {

		MtnMomoEventLog eventLog = null;
		Session session = sessionFactory.getCurrentSession();

		try {
			eventLog = (MtnMomoEventLog) session.createQuery("From MtnMomoEventLog order by eventId DESC").list()
					.get(0);
		} catch (Exception e) {
			LOGGER.error("=> Error in TransactiondaoImpl of getByLastRecord function ", e);
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(MFSMtnMomoConstant.ER234);
			status.setStatusMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER234));
			throw new DaoException(status);

		}
		return eventLog;

	}

	@Transactional
	public ValidateAccount getAccountDetailsByMsisdn(String msisdn) throws DaoException {

		ValidateAccount accountDetails = null;

		Session session = sessionFactory.getCurrentSession();
		try {
			accountDetails = (ValidateAccount) session
					.createQuery("From ValidateAccount where accountHolderId=:accountHolderId")
					.setParameter("accountHolderId", msisdn).uniqueResult();
		} catch (Exception e) {
			LOGGER.error("==>Exception in TransactionDaoImpl ", e);
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(MFSMtnMomoConstant.ER239);
			status.setStatusMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER239));
			throw new DaoException(status);
		}
		return accountDetails;
	}

	@Transactional
	public AuthDetailsModel getAuthDetailsByCountryCode(String countryCode) throws DaoException {
		AuthDetailsModel authDetailsModel = null;

		Session session = sessionFactory.getCurrentSession();
		try {
			authDetailsModel = (AuthDetailsModel) session
					.createQuery("From AuthDetailsModel where countryCode=:countryCode")
					.setParameter("countryCode", countryCode).uniqueResult();
		} catch (Exception e) {
			LOGGER.error("==>Exception in TransactionDaoImpl ", e);
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(MFSMtnMomoConstant.ER240);
			status.setStatusMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER240));
			throw new DaoException(status);
		}
		return authDetailsModel;
	}

	@Transactional
	public AuthorizationModel getAuthtokenByCountryCode(String countryCode) throws DaoException {
		AuthorizationModel authorizationModel = null;
		Session session = sessionFactory.getCurrentSession();
		try {
			authorizationModel = (AuthorizationModel) session
					.createQuery("From AuthorizationModel where countryCode=:countryCode")
					.setParameter("countryCode", countryCode).uniqueResult();
		} catch (Exception e) {
			LOGGER.error("==>Exception in TransactionDaoImpl ", e);
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(MFSMtnMomoConstant.ER240);
			status.setStatusMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER240));
			throw new DaoException(status);
		}
		return authorizationModel;
	}

}
