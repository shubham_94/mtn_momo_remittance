package com.mfs.client.mtn.momo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetRequestToPayResponseDto {

	private String amount;
	private String currency;
	private String externalId;
	private String financialTransactionId;
	private Payee payee;
	private String payerMessage;
	private String payeeNote;
	private String status;
	private String reason;
	private String code;
	private String message;

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public Payee getPayee() {
		return payee;
	}

	public void setPayee(Payee payee) {
		this.payee = payee;
	}

	public String getPayerMessage() {
		return payerMessage;
	}

	public void setPayerMessage(String payerMessage) {
		this.payerMessage = payerMessage;
	}

	public String getPayeeNote() {
		return payeeNote;
	}

	public void setPayeeNote(String payeeNote) {
		this.payeeNote = payeeNote;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	
	
	public String getFinancialTransactionId() {
		return financialTransactionId;
	}

	public void setFinancialTransactionId(String financialTransactionId) {
		this.financialTransactionId = financialTransactionId;
	}

	@Override
	public String toString() {
		return "GetRequestToPayResponseDto [amount=" + amount + ", currency=" + currency + ", externalId=" + externalId
				+ ", financialTransactionId=" + financialTransactionId + ", payee=" + payee + ", payerMessage="
				+ payerMessage + ", payeeNote=" + payeeNote + ", status=" + status + ", reason=" + reason + ", code="
				+ code + ", message=" + message + "]";
	}

	

}
