package com.mfs.client.mtn.momo.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "momo_user")
public class UserModel {

	@Id
	@GeneratedValue
	@Column(name = "user_id")
	private int userId;

	@Column(name = "provider_callback_host")
	private String providerCallbackHost;

	@Column(name = "reference_id")
	private String referenceId;

	@Column(name = "api_key")
	private String apiKey;

	@Column(name = "date_logged")
	private Date dateLogged;

	@Column(name = "target_environment")
	private String targetEnvironment;

	@Column(name = "code")
	private Integer code;

	@Column(name = "message")
	private String message;

	public String getProviderCallbackHost() {
		return providerCallbackHost;
	}

	public void setProviderCallbackHost(String providerCallbackHost) {
		this.providerCallbackHost = providerCallbackHost;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	public String getTargetEnvironment() {
		return targetEnvironment;
	}

	public void setTargetEnvironment(String targetEnvironment) {
		this.targetEnvironment = targetEnvironment;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "UserModel [userId=" + userId + ", providerCallbackHost=" + providerCallbackHost + ", referenceId="
				+ referenceId + ", apiKey=" + apiKey + ", dateLogged=" + dateLogged + ", targetEnvironment="
				+ targetEnvironment + ", code=" + code + ", message=" + message + "]";
	}

}
