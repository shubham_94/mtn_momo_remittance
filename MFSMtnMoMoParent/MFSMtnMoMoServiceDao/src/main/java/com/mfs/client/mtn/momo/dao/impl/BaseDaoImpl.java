package com.mfs.client.mtn.momo.dao.impl;

import java.util.Properties;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.client.mtn.momo.dao.BaseDao;
import com.mfs.client.mtn.momo.dto.ResponseStatus;
import com.mfs.client.mtn.momo.exception.DaoException;
import com.mfs.client.mtn.momo.util.MFSMtnMomoConstant;

public class BaseDaoImpl implements BaseDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Resource(name = "responseC")
	private Properties responseCodes;

	private static final Logger LOGGER = Logger.getLogger(BaseDaoImpl.class);

	@Transactional
	public boolean save(Object obj) throws DaoException {
		// LOGGER.debug("Inside BaseDAO Save");

		boolean isSuccess = false;

		try {
			this.sessionFactory.getCurrentSession().save(obj);
			isSuccess = true;
		} catch (Exception e) {
			LOGGER.error("==>Exception thrown in BaseDaoImpl in save ", e);
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(MFSMtnMomoConstant.ER204);
			status.setStatusMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER204));
			throw new DaoException(status);
		}
		return isSuccess;
	}

	@Transactional
	public boolean update(Object obj) throws DaoException {
		// LOGGER.debug("Inside BaseDAO Update");
		boolean isSuccess = false;
		try {
			this.sessionFactory.getCurrentSession().update(obj);
			isSuccess = true;
		} catch (Exception e) {

			LOGGER.error("==>Exception thrown in BaseDaoImpl in update", e);
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(MFSMtnMomoConstant.ER205);
			status.setStatusMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER205));
			throw new DaoException(status);
		}
		return isSuccess;
	}

	@Transactional
	public boolean saveOrUpdate(Object obj) throws DaoException {
		// LOGGER.debug("Inside BaseDAO saveOrUpdate");
		boolean isSuccess = false;
		try {
			this.sessionFactory.getCurrentSession().saveOrUpdate(obj);
			isSuccess = true;
		} catch (Exception e) {
			LOGGER.error("Exception in BaseDaoImpl in saveOrUpdate ", e);
			throw new DaoException("");
		}
		return isSuccess;
	}

	@Transactional
	public boolean delete(Object obj) throws DaoException {
		// LOGGER.debug("Inside BaseDAO delete");
		boolean isSuccess = false;
		try {
			this.sessionFactory.getCurrentSession().delete(obj);
			isSuccess = true;
		} catch (Exception e) {
			LOGGER.error("Exception in BaseDaoImpl in delete ", e);
			throw new DaoException("");
		}
		return isSuccess;
	}

}
