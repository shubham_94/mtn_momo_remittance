package com.mfs.client.mtn.momo.service;

import com.mfs.client.mtn.momo.dto.TokenResponseDto;

public interface AuthorizationTokenService {

	public TokenResponseDto createToken(String countryCode) ;

}
