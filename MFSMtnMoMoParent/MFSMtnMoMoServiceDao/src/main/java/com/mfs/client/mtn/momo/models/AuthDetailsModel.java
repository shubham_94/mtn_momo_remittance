package com.mfs.client.mtn.momo.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "momo_auth_details")
public class AuthDetailsModel {

	@Id
	@GeneratedValue
	@Column(name = "auth_details_id")
	private int authId;

	@Column(name = "country_code", length = 10)
	private String countryCode;

	@Column(name = "api_user_id", length = 500)
	private String apiUserId;

	@Column(name = "api_key", length = 500)
	private String apiKey;

	@Column(name = "subscription_key", length = 250)
	private String subscriptionKey;

	@Column(name = "x_target_environment", length = 250)
	private String xTargetEnvironment;

	@Column(name = "x_callback_url", length = 250)
	private String xCallbackUrl;

	public int getAuthId() {
		return authId;
	}

	public void setAuthId(int authId) {
		this.authId = authId;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getApiUserId() {
		return apiUserId;
	}

	public void setApiUserId(String apiUserId) {
		this.apiUserId = apiUserId;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getSubscriptionKey() {
		return subscriptionKey;
	}

	public void setSubscriptionKey(String subscriptionKey) {
		this.subscriptionKey = subscriptionKey;
	}

	public String getxTargetEnvironment() {
		return xTargetEnvironment;
	}

	public void setxTargetEnvironment(String xTargetEnvironment) {
		this.xTargetEnvironment = xTargetEnvironment;
	}

	public String getxCallbackUrl() {
		return xCallbackUrl;
	}

	public void setxCallbackUrl(String xCallbackUrl) {
		this.xCallbackUrl = xCallbackUrl;
	}

	@Override
	public String toString() {
		return "AuthDetailsModel [authId=" + authId + ", countryCode=" + countryCode + ", apiUserId=" + apiUserId
				+ ", apiKey=" + apiKey + ", subscriptionKey=" + subscriptionKey + ", xTargetEnvironment="
				+ xTargetEnvironment + ", xCallbackUrl=" + xCallbackUrl + "]";
	}

}
