package com.mfs.client.mtn.momo.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.mtn.momo.dao.MtnMoMoSystemConfigDao;
import com.mfs.client.mtn.momo.dao.TransactionDao;
import com.mfs.client.mtn.momo.dto.GetRequestToPayResponseDto;
import com.mfs.client.mtn.momo.dto.TokenResponseDto;
import com.mfs.client.mtn.momo.exception.DaoException;
import com.mfs.client.mtn.momo.models.AuthDetailsModel;
import com.mfs.client.mtn.momo.models.AuthorizationModel;
import com.mfs.client.mtn.momo.models.MtnMomoEventLog;
import com.mfs.client.mtn.momo.models.TransactionLogModel;
import com.mfs.client.mtn.momo.service.AuthorizationTokenService;
import com.mfs.client.mtn.momo.service.GetRequestToPayService;
import com.mfs.client.mtn.momo.util.CommonConstant;
import com.mfs.client.mtn.momo.util.HttpsConnectionRequest;
import com.mfs.client.mtn.momo.util.HttpsConnectionResponse;
import com.mfs.client.mtn.momo.util.HttpsConnectorImpl;
import com.mfs.client.mtn.momo.util.JSONToObjectConversion;
import com.mfs.client.mtn.momo.util.MFSMtnMomoConstant;
import com.mfs.client.mtn.momo.util.MomoCodes;
import com.mfs.client.mtn.momo.util.TokenExpirationCheck;

@Service("GetRequestToPayService")
public class GetRequestToPayServiceImpl implements GetRequestToPayService {

	@Autowired
	MtnMoMoSystemConfigDao mtnMoMoSystemConfigDao;

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	TokenExpirationCheck tokenExpirationCheck;

	@Autowired
	AuthorizationTokenService authService;

	@Resource(name = "responseC")
	private Properties responseCodes;

	private static final Logger LOGGER = Logger.getLogger(GetRequestToPayServiceImpl.class);

	public GetRequestToPayResponseDto getRequestToPay(String externalId) {
		GetRequestToPayResponseDto response = new GetRequestToPayResponseDto();
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
		HttpsConnectionResponse httpsConResult = null;
		Map<String, String> systemConfigDetails = null;
		AuthorizationModel authModel = null;
		Map<String, String> headerMap = new HashMap<String, String>();
		TokenResponseDto authResponse = new TokenResponseDto();
		String str_result = null;
		String request = null;
		AuthDetailsModel authDetails = null;

		try {
			// externalId check exist or not
			// refrenceId from db
			TransactionLogModel transactionLog = transactionDao.getTransactionLogByExternalId(externalId);

			if (transactionLog != null) {

				// get access token
				authModel = transactionDao.getAuthtokenByCountryCode(transactionLog.getCountryCode());

				// check null token
				if (authModel == null) {
					authService.createToken(transactionLog.getCountryCode());
					authModel = transactionDao.getAuthtokenByCountryCode(transactionLog.getCountryCode());
				}

				int tokenExpiredTime = CommonConstant.expirationTime;
				int tokenTime = tokenExpirationCheck.invalidSession(authModel);

				// check whether token expire or not
				if (tokenTime >= tokenExpiredTime) {

					authResponse = authService.createToken(transactionLog.getCountryCode());
					if (authResponse.getMessage() != null) {
						response.setCode(authResponse.getCode());
						response.setMessage(authResponse.getMessage());
						return response;
					}
					// get access token
					authModel = transactionDao.getAuthtokenByCountryCode(transactionLog.getCountryCode());
				}

				// get config details
				systemConfigDetails = mtnMoMoSystemConfigDao.getConfigDetailsMap();

				// check null config details
				if (systemConfigDetails == null) {
					response.setCode(MFSMtnMomoConstant.ER237);
					response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER237));
					return response;
				}

				// check port null or not
				if (null != systemConfigDetails.get(CommonConstant.PORT)) {

					connectionRequest.setPort(Integer.parseInt(systemConfigDetails.get(CommonConstant.PORT)));
				}

				connectionRequest.setHttpmethodName("GET");
				connectionRequest.setServiceUrl(systemConfigDetails.get(CommonConstant.BASE_URL)
						+ systemConfigDetails.get(CommonConstant.STATUS_URL) + transactionLog.getReferenceId());

				// get header values from auth details table
				authDetails = transactionDao.getAuthDetailsByCountryCode(transactionLog.getCountryCode());

				if (null == authDetails) {
					response.setCode(MFSMtnMomoConstant.ER242);
					response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER242));
					return response;
				}

				// set headers
				headerMap.put(CommonConstant.AUTHORIZATION, CommonConstant.BEARER + authModel.getAccessToken());
				headerMap.put(CommonConstant.TARGET_ENVIRONMENT, authDetails.getxTargetEnvironment());
				headerMap.put(CommonConstant.SUBSCRIPTION_KEY, authDetails.getSubscriptionKey());
				connectionRequest.setHeaders(headerMap);

				// log event request
				eventRequestLog(externalId, transactionLog.getReferenceId());

				LOGGER.info("GetRequestToPayServiceImpl in getRequestToPay function partner request for MfsId : "
						+ externalId);

				httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(request, connectionRequest);

				LOGGER.info(
						"GetRequestToPayServiceImpl in getRequestToPay function partner response : " + httpsConResult);

				// check null result
				if (httpsConResult == null) {
					response.setCode(MFSMtnMomoConstant.ER213);
					response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER213));
					return response;
				}

				str_result = httpsConResult.getResponseData();

				// event response log
				eventResponseLog(str_result);

				if (null != httpsConResult.getTxMessage()
						&& httpsConResult.getTxMessage().equals(MomoCodes.ER401.getMessage())) {

					authResponse = authService.createToken(transactionLog.getCountryCode());
					if (authResponse.getMessage() != null) {
						response.setCode(authResponse.getCode());
						response.setMessage(authResponse.getMessage());
						return response;
					}

					// set new token
					headerMap.put(CommonConstant.AUTHORIZATION, CommonConstant.BEARER + authResponse.getAccess_token());

					connectionRequest.setHeaders(headerMap);

					LOGGER.info("GetRequestToPayServiceImpl in getRequestToPay function partner request for MfsId : "
							+ request);

					httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(request, connectionRequest);

					LOGGER.info("GetRequestToPayServiceImpl in getRequestToPay function partner response : "
							+ httpsConResult);

					// check null result
					if (httpsConResult == null) {
						response.setCode(MFSMtnMomoConstant.ER213);
						response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER213));
						return response;
					}

					str_result = httpsConResult.getResponseData();

					// event response log
					eventResponseLog(str_result);
				}

				if (httpsConResult.getRespCode() == MomoCodes.S200.getCode()) {

					response = (GetRequestToPayResponseDto) JSONToObjectConversion.getObjectFromJson(str_result,
							GetRequestToPayResponseDto.class);

					logResponse(response, transactionLog);
				} else {
					response.setCode(httpsConResult.getCode());
					response.setMessage(httpsConResult.getTxMessage());
				}
			} else {
				response.setCode(MFSMtnMomoConstant.ER234);
				response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER234));
			}
		} catch (DaoException de) {
			LOGGER.error("==>DaoException in getRequestToPayServiceImpl", de);
			response.setCode(de.getStatus().getStatusCode());
			response.setMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>Exception in getRequestToPayServiceImpl", e);
			response.setCode(MFSMtnMomoConstant.ER203);
			response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER203));
		}
		return response;
	}

	private void eventRequestLog(String mfsId, String referenceId) throws DaoException {
		MtnMomoEventLog eventRequestLog = new MtnMomoEventLog();
		eventRequestLog.setMfsId(mfsId);
		eventRequestLog.setRequest(referenceId);
		eventRequestLog.setServiceName(CommonConstant.GET_REQUEST_TO_PAY_SERVICE);
		eventRequestLog.setDateLogged(new Date());
		transactionDao.save(eventRequestLog);
	}

	private void eventResponseLog(String response) throws DaoException {
		MtnMomoEventLog transEventLog = transactionDao.getByLastRecord();
		if (transEventLog != null) {
			transEventLog.setResponse(response);
			transEventLog.setDateLogged(new Date());
			transactionDao.update(transEventLog);
		}
	}

	private void logResponse(GetRequestToPayResponseDto response, TransactionLogModel transactionLog)
			throws DaoException {
		transactionLog.setStatus(response.getStatus());
		transactionLog.setFinancialTransactionId(response.getFinancialTransactionId());
		transactionLog.setDate(new Date());
		transactionDao.update(transactionLog);
	}

}
