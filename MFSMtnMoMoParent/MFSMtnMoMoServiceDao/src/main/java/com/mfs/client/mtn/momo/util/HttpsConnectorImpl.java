/**
 * 
 */
package com.mfs.client.mtn.momo.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ProtocolException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import org.apache.log4j.Logger;

public class HttpsConnectorImpl {

	private static final int CHECK_RESPONSE_CODE201 = 201;
	private static final int CHECK_RESPONSE_CODE200 = 200;
	private static final int CHECK_RESPONSE_CODE202 = 202;

	private static final Logger LOGGER = Logger.getLogger(HttpsConnectorImpl.class);

	/**
	 * @param connectURL
	 * @param data
	 * @return
	 * @throws UnknownHostException
	 * @throws IOException
	 * @throws ProtocolException
	 * @throws MTAdapterConEx
	 */
	public HttpsConnectionResponse connectionUsingHTTPS(String connectionData,
			final HttpsConnectionRequest httpsConnectionRequest) throws UnknownHostException, IOException, Exception {
		SSLContext sslContext = null;
		HttpsConnectionResponse connectionResponse = new HttpsConnectionResponse();
		String responseData = null;

		try {
			sslContext = SSLContext.getInstance("TLSv1.2");

			sslContext.init(null, new javax.net.ssl.TrustManager[] { new TrustAllTrustManager() },
					new java.security.SecureRandom());
		} catch (Exception e) {
			throw new Exception(e);
		}

		//

		URL httpsUrl = new URL(httpsConnectionRequest.getServiceUrl());
		HttpsURLConnection httpsConnection = (HttpsURLConnection) httpsUrl.openConnection();

		sslContext.getSocketFactory().createSocket(httpsUrl.getHost(), httpsConnectionRequest.getPort());
		httpsConnection.setSSLSocketFactory(sslContext.getSocketFactory());

		sslContext.getSocketFactory().createSocket(httpsUrl.getHost(), httpsConnectionRequest.getPort());

		setHeaderPropertiesToHTTPSConnection(httpsConnectionRequest, connectionData, httpsConnection);

		httpsConnection.setDoInput(true);
		httpsConnection.setDoOutput(true);
		httpsConnection.setReadTimeout(500000);

		if (connectionData != null) {
			DataOutputStream wr = new DataOutputStream(httpsConnection.getOutputStream());

			wr.writeBytes(connectionData);
			wr.flush();
			wr.close();
		}

		int responseCode = httpsConnection.getResponseCode();

		BufferedReader httpsResponse = null;

		if (responseCode == MomoCodes.S200.getCode() || responseCode == MomoCodes.S201.getCode()
				|| responseCode == MomoCodes.S202.getCode()) {
			connectionResponse.setCode(responseCode + "");
			httpsResponse = new BufferedReader(new InputStreamReader(httpsConnection.getInputStream()));

		} else if (responseCode == MomoCodes.ER400.getCode()) {
			if (null == httpsConnection.getErrorStream()) {

				connectionResponse.setTxMessage(MomoCodes.ER400.getMessage());

			} else {
				String errMessage = getInputAsString(httpsConnection.getErrorStream());
				LOGGER.error("ER" + responseCode + " ErrorMessage " + errMessage);
				connectionResponse.setTxMessage(errMessage);
				connectionResponse.setResponseData(errMessage);
			}
			connectionResponse.setCode("ER" + responseCode);

		} else if (responseCode == MomoCodes.ER401.getCode()) {
			if (null == httpsConnection.getErrorStream()) {

				connectionResponse.setTxMessage(MomoCodes.ER401.getMessage());

			} else {
				String errMessage = getInputAsString(httpsConnection.getErrorStream());
				LOGGER.error("ER" + responseCode + " ErrorMessage " + errMessage);
				connectionResponse.setTxMessage(errMessage);
				connectionResponse.setResponseData(errMessage);

			}
			connectionResponse.setCode("ER" + responseCode);

		} else if (responseCode == MomoCodes.ER405.getCode()) {

			if (null == httpsConnection.getErrorStream()) {

				connectionResponse.setTxMessage(MomoCodes.ER405.getMessage());

			} else {
				String errMessage = getInputAsString(httpsConnection.getErrorStream());
				LOGGER.error("ER" + responseCode + " ErrorMessage " + errMessage);
				connectionResponse.setTxMessage(errMessage);
				connectionResponse.setResponseData(errMessage);
			}
			connectionResponse.setCode("ER" + responseCode);

		} else if (responseCode == MomoCodes.ER409.getCode()) {

			if (null == httpsConnection.getErrorStream()) {

				connectionResponse.setTxMessage(MomoCodes.ER409.getMessage());

			} else {
				String errMessage = getInputAsString(httpsConnection.getErrorStream());
				LOGGER.error("ER" + responseCode + " ErrorMessage " + errMessage);
				connectionResponse.setTxMessage(errMessage);
				connectionResponse.setResponseData(errMessage);
			}
			connectionResponse.setCode("ER" + responseCode);

		} else if (responseCode == MomoCodes.ER500.getCode()) {

			if (null == httpsConnection.getErrorStream()) {

				connectionResponse.setTxMessage(MomoCodes.ER500.getMessage());

			} else {
				String errMessage = getInputAsString(httpsConnection.getErrorStream());
				LOGGER.error("ER" + responseCode + " ErrorMessage " + errMessage);
				connectionResponse.setTxMessage(errMessage);
				connectionResponse.setResponseData(errMessage);
			}
			connectionResponse.setCode("ER" + responseCode);

		} else if (responseCode == MomoCodes.ER404.getCode()) {

			if (null == httpsConnection.getErrorStream()) {

				connectionResponse.setTxMessage(MomoCodes.ER404.getMessage());

			} else {
				String errMessage = getInputAsString(httpsConnection.getErrorStream());
				LOGGER.error("ER" + responseCode + " ErrorMessage " + errMessage);
				connectionResponse.setTxMessage(errMessage);
				connectionResponse.setResponseData(errMessage);
			}
			connectionResponse.setCode("ER" + responseCode);

		} else if (responseCode == MomoCodes.ER403.getCode()) {

			if (null == httpsConnection.getErrorStream()) {

				connectionResponse.setTxMessage(MomoCodes.ER403.getMessage());

			} else {
				String errMessage = getInputAsString(httpsConnection.getErrorStream());
				LOGGER.error("ER" + responseCode + " ErrorMessage " + errMessage);
				connectionResponse.setTxMessage(errMessage);
				connectionResponse.setResponseData(errMessage);
			}
			connectionResponse.setCode("ER" + responseCode);

		} else {

			if (null == httpsConnection.getErrorStream()) {

				connectionResponse.setTxMessage(CommonConstant.PARTNER_SYSTEM_ERROR);

			} else {
				String errMessage = getInputAsString(httpsConnection.getErrorStream());
				LOGGER.error("ER" + responseCode + " ErrorMessage " + errMessage);
				// update txMessage after getting actual error response
				connectionResponse.setTxMessage(errMessage);
				connectionResponse.setResponseData(errMessage);
			}
			connectionResponse.setCode("ER" + responseCode);

		}

		String output = null;
		if (httpsResponse != null) {
			output = readConnectionDataWithBuffer(httpsResponse);
		}

		if (httpsResponse != null) {
			httpsResponse.close();
		}

		if (responseCode == MomoCodes.S201.getCode() && output.equals("")) {
			connectionResponse.setTxMessage(MomoCodes.S200.getMessage());
			connectionResponse.setRespCode(responseCode);
		}
		if (responseCode == MomoCodes.S202.getCode() && output.equals("")) {
			connectionResponse.setTxMessage(MomoCodes.S202.getMessage());
			connectionResponse.setRespCode(responseCode);
		} else if (output != null) {
			connectionResponse.setResponseData(output);
			connectionResponse.setRespCode(responseCode);
			connectionResponse.setTxMessage(MomoCodes.S200.getMessage());
		}

		return connectionResponse;
	}

	private static String getInputAsString(InputStream paramInputStream) {
		Scanner localScanner = new Scanner(paramInputStream);
		return localScanner.useDelimiter("\\A").hasNext() ? localScanner.next() : "";
	}

	/**
	 * @param connectURL
	 * @param data
	 * @return
	 * @throws UnknownHostException
	 * @throws IOException
	 * @throws ProtocolException
	 * @throws MTAdapterConEx
	 */
	private BufferedReader connectionUsingHTTPSRetry(String connectionData,
			final HttpsConnectionRequest httpsConnectionRequest) throws UnknownHostException, IOException {
		SSLContext sslContext = null;

		try {
			sslContext = SSLContext.getInstance("SSL");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
		}
		try {
			sslContext.init(null, new javax.net.ssl.TrustManager[] { new TrustAllTrustManager() },
					new java.security.SecureRandom());
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
		}

		URL httpsUrl = new URL(httpsConnectionRequest.getServiceUrl());

		sslContext.getSocketFactory().createSocket(httpsUrl.getHost(), httpsConnectionRequest.getPort());

		HttpsURLConnection httpsConnection = (HttpsURLConnection) httpsUrl.openConnection();

		httpsConnection.setSSLSocketFactory(sslContext.getSocketFactory());

		httpsConnection.setRequestMethod(httpsConnectionRequest.getHttpmethodName());

		// Read timeout Changed to 60 sec
		httpsConnection.setReadTimeout(60000);

		// Send post request
		httpsConnection.setDoOutput(true);
		httpsConnection.setDoInput(true);

		setHeaderPropertiesToHTTPSConnection(httpsConnectionRequest, connectionData, httpsConnection);

		if (connectionData != null) {

			DataOutputStream wr = new DataOutputStream(httpsConnection.getOutputStream());

			wr.writeBytes(connectionData);
			wr.flush();
			wr.close();
		}
		int responseCode = httpsConnection.getResponseCode();

		BufferedReader httpsResponse = null;

		if (responseCode == CHECK_RESPONSE_CODE200 || responseCode == CHECK_RESPONSE_CODE201
				|| responseCode == CHECK_RESPONSE_CODE202) {

			httpsResponse = new BufferedReader(new InputStreamReader(httpsConnection.getInputStream()));

		} else {

			httpsResponse = new BufferedReader(new InputStreamReader(httpsConnection.getErrorStream()));

		}

		String output = readConnectionDataWithBuffer(httpsResponse);

		if (output == null || output.isEmpty()) {
			if (responseCode == CHECK_RESPONSE_CODE201 || responseCode == CHECK_RESPONSE_CODE200
					|| responseCode == CHECK_RESPONSE_CODE202) {
				output = "OK";
			}
		}

		return httpsResponse;
	}

	public static void wait(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
		}
	}

	/**
	 * @param httpsResponse
	 * @return
	 * @throws IOException
	 */
	private String readConnectionDataWithBuffer(BufferedReader httpsResponse) throws IOException {
		String output;
		String responseLine;
		StringBuffer response = new StringBuffer();

		while ((responseLine = httpsResponse.readLine()) != null) {
			response.append(responseLine);
		}

		output = response.toString();
		return output;
	}

	/**
	 * Set Header Properties To HTTPS Connection
	 * 
	 * @param httpsConnectionRequest
	 * @param connectionData
	 * @param httpsConnection
	 */
	private void setHeaderPropertiesToHTTPSConnection(final HttpsConnectionRequest httpsConnectionRequest,
			String connectionData, HttpsURLConnection httpsConnection) {

		setHeadersToHTTPSConnection(httpsConnectionRequest.getHeaders(), httpsConnection);

		if (connectionData != null && connectionData.length() > 0) {

			httpsConnection.setRequestProperty("Content-Length", String.valueOf(connectionData.length()));
		}

	}

	/**
	 * Set headers that are required by calling app
	 * 
	 * @param headers
	 * @param httpsConnection
	 */
	private void setHeadersToHTTPSConnection(Map<String, String> headers, HttpsURLConnection httpsConnection) {

		if (headers != null && !headers.isEmpty() && httpsConnection != null) {

			for (Entry<String, String> entry : headers.entrySet()) {

				httpsConnection.setRequestProperty(entry.getKey(), entry.getValue());
			}
		}

	}

}