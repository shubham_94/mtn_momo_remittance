package com.mfs.client.mtn.momo.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "momo_transaction_log")
public class TransactionLogModel {

	@Id
	@GeneratedValue
	@Column(name = "transaction_id")
	private int transactionId;

	@Column(name = "amount")
	private String amount;

	@Column(name = "currency")
	private String currency;

	@Column(name = "external_id")
	private String externalId;

	@Column(name = "payer_party_id_type")
	private String payerPartyIdType;

	@Column(name = "payer_party_id")
	private String payerPartyId;

	@Column(name = "payer_message")
	private String payerMessage;

	@Column(name = "payee_note")
	private String payeeNote;

	@Column(name = "code")
	private String code;

	@Column(name = "message")
	private String message;

	@Column(name = "status")
	private String status;

	@Column(name = "reason")
	private String reason;

	@Column(name = "date_logged")
	private Date date;

	@Column(name = "reference_id")
	private String referenceId;

	@Column(name = "financial_transactionId")
	private String financialTransactionId;

	@Column(name = "country_code")
	private String countryCode;

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getPayerPartyIdType() {
		return payerPartyIdType;
	}

	public void setPayerPartyIdType(String payerPartyIdType) {
		this.payerPartyIdType = payerPartyIdType;
	}

	public String getPayerPartyId() {
		return payerPartyId;
	}

	public void setPayerPartyId(String payerPartyId) {
		this.payerPartyId = payerPartyId;
	}

	public String getPayerMessage() {
		return payerMessage;
	}

	public void setPayerMessage(String payerMessage) {
		this.payerMessage = payerMessage;
	}

	public String getPayeeNote() {
		return payeeNote;
	}

	public void setPayeeNote(String payeeNote) {
		this.payeeNote = payeeNote;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getFinancialTransactionId() {
		return financialTransactionId;
	}

	public void setFinancialTransactionId(String financialTransactionId) {
		this.financialTransactionId = financialTransactionId;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	@Override
	public String toString() {
		return "TransactionLogModel [transactionId=" + transactionId + ", amount=" + amount + ", currency=" + currency
				+ ", externalId=" + externalId + ", payerPartyIdType=" + payerPartyIdType + ", payerPartyId="
				+ payerPartyId + ", payerMessage=" + payerMessage + ", payeeNote=" + payeeNote + ", code=" + code
				+ ", message=" + message + ", status=" + status + ", reason=" + reason + ", date=" + date
				+ ", referenceId=" + referenceId + ", financialTransactionId=" + financialTransactionId
				+ ", countryCode=" + countryCode + "]";
	}

}
