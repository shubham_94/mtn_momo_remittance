package com.mfs.client.mtn.momo.dao;

import com.mfs.client.mtn.momo.exception.DaoException;
import com.mfs.client.mtn.momo.models.AuthDetailsModel;
import com.mfs.client.mtn.momo.models.AuthorizationModel;
import com.mfs.client.mtn.momo.models.MtnMomoEventLog;
import com.mfs.client.mtn.momo.models.TransactionLogModel;
import com.mfs.client.mtn.momo.models.UserModel;
import com.mfs.client.mtn.momo.models.ValidateAccount;

public interface TransactionDao extends BaseDao {

	UserModel getTransactionLogByreferenceId(String referenceId) throws DaoException;

	TransactionLogModel getTransactionLogByExtrernalId(String externalId) throws DaoException;

	TransactionLogModel getTransactionLogByReferenceId(String referenceId) throws DaoException;

	UserModel getLastRecordByreferenceId() throws DaoException;

	AuthorizationModel getLastRecordByAccessToken() throws DaoException;

	TransactionLogModel getTransactionLogByExternalId(String externalId) throws DaoException;

	MtnMomoEventLog getByLastRecord() throws DaoException;
	
	public ValidateAccount getAccountDetailsByMsisdn(String msisdn) throws DaoException ;

	AuthDetailsModel getAuthDetailsByCountryCode(String countryCode)throws DaoException;

	AuthorizationModel getAuthtokenByCountryCode(String countryCode)throws DaoException;


}
