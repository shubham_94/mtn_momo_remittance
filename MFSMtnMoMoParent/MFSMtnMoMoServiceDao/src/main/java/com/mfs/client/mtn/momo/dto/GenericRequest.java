package com.mfs.client.mtn.momo.dto;
/**
 * 
 */

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GenericRequest {
	private String username;
	private String password;
	private String partnerCode;
	private String amount;
	private String currencyCode;
	private String reference;
	private String mfsTransactionId;
	private String methodName;
	private String senderMsisdn;
	private String senderAccount_no;
	private String senderName;
	private String recipientMsisdn;
	private String recipientName;
	private String sendCountry;
	@JsonProperty("callbackUrl")
	private String callbackUrl;
	private Map<String, String> additionalParameter = new HashMap<String, String>();

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getMfsTransactionId() {
		return mfsTransactionId;
	}

	public void setMfsTransactionId(String mfsTransactionId) {
		this.mfsTransactionId = mfsTransactionId;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getSenderMsisdn() {
		return senderMsisdn;
	}

	public void setSenderMsisdn(String senderMsisdn) {
		this.senderMsisdn = senderMsisdn;
	}

	public String getSenderAccount_no() {
		return senderAccount_no;
	}

	public void setSenderAccount_no(String senderAccount_no) {
		this.senderAccount_no = senderAccount_no;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getRecipientMsisdn() {
		return recipientMsisdn;
	}

	public void setRecipientMsisdn(String recipientMsisdn) {
		this.recipientMsisdn = recipientMsisdn;
	}

	public String getRecipientName() {
		return recipientName;
	}

	public void setRecipientName(String recipientName) {
		this.recipientName = recipientName;
	}

	public String getSendCountry() {
		return sendCountry;
	}

	public void setSendCountry(String sendCountry) {
		this.sendCountry = sendCountry;
	}

	public Map<String, String> getAdditionalParameter() {
		return additionalParameter;
	}

	public void setAdditionalParameter(Map<String, String> additionalParameter) {
		this.additionalParameter = additionalParameter;
	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GenericRequest [username=");
		builder.append(username);
		builder.append(", password=");
		builder.append(password);
		builder.append(", partnerCode=");
		builder.append(partnerCode);
		builder.append(", amount=");
		builder.append(amount);
		builder.append(", currencyCode=");
		builder.append(currencyCode);
		builder.append(", reference=");
		builder.append(reference);
		builder.append(", mfsTransactionId=");
		builder.append(mfsTransactionId);
		builder.append(", methodName=");
		builder.append(methodName);
		builder.append(", senderMsisdn=");
		builder.append(senderMsisdn);
		builder.append(", senderAccount_no=");
		builder.append(senderAccount_no);
		builder.append(", senderName=");
		builder.append(senderName);
		builder.append(", recipientMsisdn=");
		builder.append(recipientMsisdn);
		builder.append(", recipientName=");
		builder.append(recipientName);
		builder.append(", sendCountry=");
		builder.append(sendCountry);
		builder.append(", callbackUrl=");
		builder.append(callbackUrl);
		builder.append(", additionalParameter=");
		builder.append(additionalParameter);
		builder.append("]");
		return builder.toString();
	}

}
