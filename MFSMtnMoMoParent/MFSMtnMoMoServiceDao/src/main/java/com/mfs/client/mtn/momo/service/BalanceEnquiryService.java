package com.mfs.client.mtn.momo.service;

import com.mfs.client.mtn.momo.dto.BalanceResponseDto;

public interface BalanceEnquiryService {
 
	BalanceResponseDto balanceService(String countryCode);
}