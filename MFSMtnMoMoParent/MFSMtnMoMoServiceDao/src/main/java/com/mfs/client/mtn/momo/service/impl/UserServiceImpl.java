package com.mfs.client.mtn.momo.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.client.mtn.momo.dao.MtnMoMoSystemConfigDao;
import com.mfs.client.mtn.momo.dao.TransactionDao;
import com.mfs.client.mtn.momo.dto.UserRequestDto;
import com.mfs.client.mtn.momo.dto.UserResponse;
import com.mfs.client.mtn.momo.exception.DaoException;
import com.mfs.client.mtn.momo.models.MtnMomoEventLog;
import com.mfs.client.mtn.momo.models.UserModel;
import com.mfs.client.mtn.momo.service.UserService;
import com.mfs.client.mtn.momo.util.CommonConstant;
import com.mfs.client.mtn.momo.util.HttpsConnectionRequest;
import com.mfs.client.mtn.momo.util.HttpsConnectionResponse;
import com.mfs.client.mtn.momo.util.HttpsConnectorImpl;
import com.mfs.client.mtn.momo.util.MFSMtnMomoConstant;
import com.mfs.client.mtn.momo.util.MomoCodes;

@Service("UserService")
public class UserServiceImpl implements UserService {

	@Autowired
	MtnMoMoSystemConfigDao mtnMoMoSystemConfigDao;

	@Autowired
	TransactionDao transactionDao;

	@Resource(name = "responseC")
	private Properties responseCodes;

	private static final Logger LOGGER = Logger.getLogger(UserServiceImpl.class);

	public UserResponse userService(UserRequestDto requestDto) {

		UserResponse response = new UserResponse();
		Map<String, String> configMap;
		Gson gson = new Gson();
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		Map<String, String> headerMap = new HashMap<String, String>();
		HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();

		try {

			HttpsConnectionResponse httpsConResult = null;

			// get reference id
			UUID uuid = UUID.randomUUID();
			String referenceId = uuid.toString();

			logUserRequest(requestDto, referenceId);

			// check comfig details
			configMap = mtnMoMoSystemConfigDao.getConfigDetailsMap();
			if (configMap == null) {
				response.setCode(MFSMtnMomoConstant.ER237);
				response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER237));
				return response;
			}

			headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
			headerMap.put(CommonConstant.REFERENCE_ID, referenceId);
			headerMap.put(CommonConstant.SUBSCRIPTION_KEY, configMap.get(CommonConstant.SUBSCRIPTIONKEY));
			connectionRequest.setHeaders(headerMap);
			connectionRequest.setHttpmethodName("POST");

			// check port null or not
			if (null != configMap.get(CommonConstant.PORT)) {
				connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)));
			}

			connectionRequest
					.setServiceUrl(configMap.get(CommonConstant.SANDBOX_PROVISIONING_URL) + configMap.get(CommonConstant.USER_URL));

			String request = gson.toJson(requestDto);

			// log event request
			eventRequestLog(request);

			LOGGER.info("userServiceImpl in userService function partner request : " + request);
			httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(request, connectionRequest);
			LOGGER.info("userServiceImpl in userService function partner response : " + httpsConResult);

			// check null result
			if (httpsConResult == null) {
				response.setCode(MFSMtnMomoConstant.ER213);
				response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER213));
				return response;
			}

			// event response log
			eventResponseLog(httpsConResult.getCode());

			// condition to handle success and fail response
			if (httpsConResult.getRespCode() == MomoCodes.S201.getCode()) {
				updateResponse(requestDto, httpsConResult, referenceId);
				response.setCode(Integer.toString(httpsConResult.getRespCode()));
				response.setMessage(httpsConResult.getTxMessage());
				// LOGGER.debug("userServiceImpl in userService function response " + response);

			} else {
				response.setCode(httpsConResult.getCode());
				response.setMessage(httpsConResult.getTxMessage());
			}

		} catch (DaoException de) {
			LOGGER.error("==>DaoException in userServiceImpl", de);
			response.setCode(de.getStatus().getStatusCode());
			response.setMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>Exception in userServiceImpl", e);
			response.setCode(MFSMtnMomoConstant.ER203);
			response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER203));
		}
		return response;
	}

	private void eventResponseLog(String response) throws DaoException {
		MtnMomoEventLog transEventLog = transactionDao.getByLastRecord();
		if (transEventLog != null) {
			transEventLog.setResponse(response);
			transEventLog.setDateLogged(new Date());
			transactionDao.update(transEventLog);

		}

	}

	private void eventRequestLog(String request) throws DaoException {

		MtnMomoEventLog eventRequestLog = new MtnMomoEventLog();
		eventRequestLog.setMfsId(null);
		eventRequestLog.setRequest(request);
		eventRequestLog.setServiceName(CommonConstant.USER_SERVICE);
		eventRequestLog.setDateLogged(new Date());
		transactionDao.save(eventRequestLog);
	}

	// to update response against referenceId in db
	private void updateResponse(UserRequestDto requestDto, HttpsConnectionResponse httpsConResult, String referenceId)
			throws DaoException {

		UserModel userModel = transactionDao.getTransactionLogByreferenceId(referenceId);
		if (userModel != null) {
			userModel.setCode(httpsConResult.getRespCode());
			userModel.setMessage(httpsConResult.getTxMessage());
			userModel.setDateLogged(new Date());
			transactionDao.update(userModel);
		}

	}

	// to log request in db
	private void logUserRequest(UserRequestDto requestDto, String referenceId) throws DaoException {

		UserModel userModel = new UserModel();
		userModel.setProviderCallbackHost(requestDto.getProviderCallbackHost());
		userModel.setReferenceId(referenceId);
		userModel.setDateLogged(new Date());
		transactionDao.save(userModel);
	}
}
