package com.mfs.client.mtn.momo.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "momo_validate_account")
public class ValidateAccount {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private int id;

	@Column(name = "msisdn")
	private String accountHolderId;

	@Column(name = "sub")
	private String sub;

	@Column(name = "name")
	private String name;

	@Column(name = "given_name")
	private String givenName;

	@Column(name = "family_name")
	private String familyName;

	@Column(name = "birthdate")
	private String birthdate;

	@Column(name = "locale")
	private String locale;

	@Column(name = "gender")
	private String gender;

	@Column(name = "updated_at")
	private String updatedAt;

	@Column(name = "date_logged")
	private Date dateLogged;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccountHolderId() {
		return accountHolderId;
	}

	public void setAccountHolderId(String accountHolderId) {
		this.accountHolderId = accountHolderId;
	}

	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	@Override
	public String toString() {
		return "ValidateAccount [id=" + id + ", accountHolderId=" + accountHolderId + ", sub=" + sub + ", name=" + name
				+ ", givenName=" + givenName + ", familyName=" + familyName + ", birthdate=" + birthdate + ", locale="
				+ locale + ", gender=" + gender + ", updatedAt=" + updatedAt + ", dateLogged=" + dateLogged + "]";
	}

}
