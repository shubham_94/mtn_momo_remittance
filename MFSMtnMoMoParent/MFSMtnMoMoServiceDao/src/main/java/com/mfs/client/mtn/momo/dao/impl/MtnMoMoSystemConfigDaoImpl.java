package com.mfs.client.mtn.momo.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.client.mtn.momo.dao.MtnMoMoSystemConfigDao;
import com.mfs.client.mtn.momo.dto.ResponseStatus;
import com.mfs.client.mtn.momo.exception.DaoException;
import com.mfs.client.mtn.momo.models.SystemConfigModel;
import com.mfs.client.mtn.momo.util.MFSMtnMomoConstant;

@EnableTransactionManagement
@Repository("MtnMoMoSystemConfigDao")
@Component
public class MtnMoMoSystemConfigDaoImpl implements MtnMoMoSystemConfigDao {

	private static final Logger LOGGER = Logger.getLogger(MtnMoMoSystemConfigDaoImpl.class);

	@Autowired
	SessionFactory sessionFactory;

	@Resource(name = "responseC")
	private Properties responseCodes;

	@Transactional
	public Map<String, String> getConfigDetailsMap() throws DaoException {

		LOGGER.debug("Inside getConfigDetailsMap of MtnMoMoSystemConfigDaoImpl");

		Map<String, String> congoSystemConfigurationMap = null;

		try {
			Session session = sessionFactory.getCurrentSession();

			String hql = " From SystemConfigModel";
			Query query = session.createQuery(hql);
			List<SystemConfigModel> systemConfigModel = query.list();

			if (systemConfigModel != null && !systemConfigModel.isEmpty()) {
				congoSystemConfigurationMap = new HashMap<String, String>();
				for (SystemConfigModel amCongoSystemConfiguration : systemConfigModel) {
					congoSystemConfigurationMap.put(amCongoSystemConfiguration.getConfigKey(),
							amCongoSystemConfiguration.getConfigValue());
				}
			}
			/*
			 * if (congoSystemConfigurationMap != null &&
			 * !congoSystemConfigurationMap.isEmpty()) {
			 * LOGGER.debug("getConfigDetailsMap response size -> " +
			 * congoSystemConfigurationMap.size());
			 * 
			 * }
			 */

		} catch (Exception e) {
			LOGGER.error("==>Exception in SystemConfigDaoImpl", e);
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(MFSMtnMomoConstant.ER206);
			status.setStatusMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER206));
			throw new DaoException(status);
		}
		return congoSystemConfigurationMap;
	}
}

/*
 * catch (Exception e) { LOGGER.error("==>Exception in SystemConfigDaoImpl", e);
 * ResponseStatus status = new ResponseStatus();
 * status.setStatusMessage(responseCodes.getProperty(MFSMgrushConstant.ER213));
 * status.setStatusCode(MFSMgrushConstant.ER213); throw new
 * DaoException(status); }
 */
