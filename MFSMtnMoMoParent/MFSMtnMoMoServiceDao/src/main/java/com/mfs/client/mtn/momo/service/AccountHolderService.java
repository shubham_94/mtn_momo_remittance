package com.mfs.client.mtn.momo.service;

import com.mfs.client.mtn.momo.dto.ValidateAccountResponseDto;

public interface AccountHolderService {

	ValidateAccountResponseDto getAccountHolderService(String msisdn,String countryCode);
}
