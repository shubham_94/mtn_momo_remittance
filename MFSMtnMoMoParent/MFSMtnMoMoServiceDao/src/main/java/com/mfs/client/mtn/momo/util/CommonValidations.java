package com.mfs.client.mtn.momo.util;

import java.util.regex.Pattern;

public class CommonValidations {
	public boolean validateStringValues(String reqParamValue) {
		boolean validateResult = false;
		if (reqParamValue == null || reqParamValue.equals("") || reqParamValue.equals("0"))
			validateResult = true;

		return validateResult;
	}

	public boolean validateAmountValues(Double amount) {
		boolean validateResult = false;
		if (amount == null || amount <= 0 )
			validateResult = true;

		return validateResult;
	}

	public boolean validateForCountryCode(String number) {
		boolean validateResult = false;
		if (number == null || number.equals(" ") || !number.matches("[A-Z]{3}"))
			validateResult = true;
		return validateResult;

	}

	/*
	 * public boolean validateMsisdn1(String msisdn) { boolean result=false;
	 * 
	 * Pattern pattern = Pattern.compile("^\\+(?:[0-9] ?){6,14}[0-9]$"); Pattern
	 * pattern1 =
	 * Pattern.compile("^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$");
	 * if (pattern.matcher(msisdn).matches()||pattern1.matcher(msisdn).matches()){
	 * result=true; } return result; }
	 */
	public boolean validateMsisdn1(String msisdn) {
		boolean result = false;

		// Pattern pattern = Pattern.compile("^\\+(?:[0-9] ?){6,14}[0-9]$");
		// Pattern pattern1 =
		// Pattern.compile("^\\+\\d{1,3}\\s\\d{1,14}(\\s\\d{1,13})?");
		Pattern pattern2 = Pattern.compile("^[0-9]+$");
		if (!pattern2.matcher(msisdn).matches()) {
			result = true;
		}
		return result;
	}

	public boolean validateEmail1(String email) {
		boolean result = false;
		Pattern pattern = Pattern.compile("^(.+)@(.+)$");
		if (!pattern.matcher(email).matches()) {
			result = true;
		}
		return result;
	}

	public boolean validateStringForLowerCase(String reqParamValue) {
		boolean validateResult = false;
		reqParamValue=reqParamValue.replace("_", "");
		Pattern pattern = Pattern.compile("^[a-z]+$");
		if (reqParamValue == null || reqParamValue.equals(" ") || !pattern.matcher(reqParamValue).matches())
			validateResult = true;
		return validateResult;
	}



	public boolean validateUUID(String uuid) {
		boolean result = false;
		Pattern pattern1 = Pattern.compile("[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}");

		Pattern pattern2 = Pattern.compile("([a-f0-9]{8}-(?:[a-f0-9]{4}-){3}[a-f0-9]{12}){1}");

		if (!pattern1.matcher(uuid).matches() || !pattern2.matcher(uuid).matches() ) {
			result = true;
		}
		return result;
	}
	
	public boolean validateAmount(String amount) {
		boolean validateResult = false;
		if (null == amount || amount.equals(" ") || amount.equals("") || Double.parseDouble(amount) <= 0)
			validateResult = true;
		return validateResult;
	}
}
