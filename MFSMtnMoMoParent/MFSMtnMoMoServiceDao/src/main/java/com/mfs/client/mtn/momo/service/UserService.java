package com.mfs.client.mtn.momo.service;

import com.mfs.client.mtn.momo.dto.UserRequestDto;
import com.mfs.client.mtn.momo.dto.UserResponse;

public interface UserService {

	UserResponse userService(UserRequestDto requestDto);

}
