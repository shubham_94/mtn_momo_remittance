package com.mfs.client.mtn.momo.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.mtn.momo.dao.MtnMoMoSystemConfigDao;
import com.mfs.client.mtn.momo.dao.TransactionDao;
import com.mfs.client.mtn.momo.dto.GetUserResponseDto;
import com.mfs.client.mtn.momo.exception.DaoException;
import com.mfs.client.mtn.momo.models.MtnMomoEventLog;
import com.mfs.client.mtn.momo.models.UserModel;
import com.mfs.client.mtn.momo.service.GetUserService;
import com.mfs.client.mtn.momo.util.CommonConstant;
import com.mfs.client.mtn.momo.util.HttpsConnectionRequest;
import com.mfs.client.mtn.momo.util.HttpsConnectionResponse;
import com.mfs.client.mtn.momo.util.HttpsConnectorImpl;
import com.mfs.client.mtn.momo.util.JSONToObjectConversion;
import com.mfs.client.mtn.momo.util.MFSMtnMomoConstant;
import com.mfs.client.mtn.momo.util.MomoCodes;

@Service("GetUserService")
public class GetUserServiceImpl implements GetUserService {

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	MtnMoMoSystemConfigDao mtnMoMoSystemConfigDao;

	@Resource(name = "responseC")
	private Properties responseCodes;

	private static final Logger LOGGER = Logger.getLogger(GetUserServiceImpl.class);

	public GetUserResponseDto getUserService() {

		Map<String, String> headerMap = new HashMap<String, String>();
		UserModel userModel;
		HttpsConnectionResponse httpsConResult = null;
		String str_result = null;
		GetUserResponseDto responseDto = new GetUserResponseDto();
		String request = null;
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		Map<String, String> configMap = null;
		HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();

		try {
			// get latest reference id of user using this method
			userModel = transactionDao.getLastRecordByreferenceId();

			// check use model null or not
			if (userModel == null) {
				responseDto = new GetUserResponseDto();
				responseDto.setCode(MFSMtnMomoConstant.ER220);
				responseDto.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER220));
			} else {

				// get config details
				configMap = mtnMoMoSystemConfigDao.getConfigDetailsMap();

				// check config details
				if (configMap == null) {
					responseDto.setCode(MFSMtnMomoConstant.ER237);
					responseDto.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER237));
					return responseDto;
				}

				// check port null or not
				connectionRequest.setHttpmethodName("GET");
				if (null != configMap.get(CommonConstant.PORT)) {
					connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)));
				}

				connectionRequest.setServiceUrl(configMap.get(CommonConstant.SANDBOX_PROVISIONING_URL)
						+ configMap.get(CommonConstant.USER_URL) + "/" + userModel.getReferenceId());
				headerMap.put(CommonConstant.SUBSCRIPTION_KEY, configMap.get(CommonConstant.SUBSCRIPTIONKEY));
				connectionRequest.setHeaders(headerMap);

				// log event request
				eventRequestLog(request);
				LOGGER.info("GetUserServiceImpl in getUserService function partner response : " + request);
				httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(request, connectionRequest);
				LOGGER.info("GetUserServiceImpl in getUserService function partner response : " + httpsConResult);

				// check null result
				if (httpsConResult == null) {
					responseDto.setCode(MFSMtnMomoConstant.ER213);
					responseDto.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER213));
					return responseDto;
				}

				str_result = httpsConResult.getResponseData();

				// event response log
				eventResponseLog(str_result);

				// check for success and fail response
				if (httpsConResult.getRespCode() == MomoCodes.S200.getCode()) {

					responseDto = (GetUserResponseDto) JSONToObjectConversion.getObjectFromJson(str_result,
							GetUserResponseDto.class);

					updateResponse(responseDto, userModel);

				} else {
					responseDto.setCode(httpsConResult.getCode());
					responseDto.setMessage(httpsConResult.getTxMessage());
				}
			}

		} catch (DaoException de) {
			LOGGER.error("==>DaoException in GetUserServiceImpl", de);
			responseDto.setCode(de.getStatus().getStatusCode());
			responseDto.setMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>Exception in GetUserServiceImpl", e);
			responseDto.setCode(MFSMtnMomoConstant.ER203);
			responseDto.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER203));
		}
		return responseDto;
	}

	private void eventResponseLog(String response) throws DaoException {

		MtnMomoEventLog transEventLog = transactionDao.getByLastRecord();
		if (transEventLog != null) {
			transEventLog.setResponse(response);
			transEventLog.setDateLogged(new Date());
			transactionDao.update(transEventLog);

		}
	}

	private void eventRequestLog(String request) throws DaoException {
		MtnMomoEventLog eventRequestLog = new MtnMomoEventLog();
		eventRequestLog.setMfsId(null);
		eventRequestLog.setRequest(request);
		eventRequestLog.setServiceName(CommonConstant.GET_USER_SERVICE);
		eventRequestLog.setDateLogged(new Date());
		transactionDao.save(eventRequestLog);
	}

	// update get user response
	private void updateResponse(GetUserResponseDto responseDto, UserModel userModel) throws DaoException {
		userModel.setProviderCallbackHost(responseDto.getProviderCallbackHost());
		userModel.setTargetEnvironment(responseDto.getTargetEnvironment());
		userModel.setDateLogged(new Date());
		transactionDao.update(userModel);

	}
}
