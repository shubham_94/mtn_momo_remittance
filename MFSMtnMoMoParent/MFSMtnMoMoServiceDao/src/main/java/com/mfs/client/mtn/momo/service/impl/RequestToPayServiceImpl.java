package com.mfs.client.mtn.momo.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.client.mtn.momo.dao.MtnMoMoSystemConfigDao;
import com.mfs.client.mtn.momo.dao.TransactionDao;
import com.mfs.client.mtn.momo.dto.Payee;
import com.mfs.client.mtn.momo.dto.RequestToPayRequestDto;
import com.mfs.client.mtn.momo.dto.RequestToPayResponseDto;
import com.mfs.client.mtn.momo.dto.TokenResponseDto;
import com.mfs.client.mtn.momo.exception.DaoException;
import com.mfs.client.mtn.momo.models.AuthDetailsModel;
import com.mfs.client.mtn.momo.models.AuthorizationModel;
import com.mfs.client.mtn.momo.models.MtnMomoEventLog;
import com.mfs.client.mtn.momo.models.TransactionErrorModel;
import com.mfs.client.mtn.momo.models.TransactionLogModel;
import com.mfs.client.mtn.momo.service.AuthorizationTokenService;
import com.mfs.client.mtn.momo.service.RequestToPayService;
import com.mfs.client.mtn.momo.util.CommonConstant;
import com.mfs.client.mtn.momo.util.HttpsConnectionRequest;
import com.mfs.client.mtn.momo.util.HttpsConnectionResponse;
import com.mfs.client.mtn.momo.util.HttpsConnectorImpl;
import com.mfs.client.mtn.momo.util.MFSMtnMomoConstant;
import com.mfs.client.mtn.momo.util.MomoCodes;
import com.mfs.client.mtn.momo.util.TokenExpirationCheck;

@Service("RequestToPayService")
public class RequestToPayServiceImpl implements RequestToPayService {

	@Autowired
	MtnMoMoSystemConfigDao mtnMoMoSystemConfigDao;

	@Autowired
	TokenExpirationCheck tokenExpirationCheck;

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	AuthorizationTokenService authService;

	@Resource(name = "responseC")
	private Properties responseCodes;

	private static final Logger LOGGER = Logger.getLogger(RequestToPayServiceImpl.class);

	public RequestToPayResponseDto requestToPay(RequestToPayRequestDto request) {

		RequestToPayResponseDto response = new RequestToPayResponseDto();
		Gson gson = new Gson();
		AuthorizationModel authModel = null;
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		HttpsConnectionResponse httpsConResult = null;
		TokenResponseDto authResponse = new TokenResponseDto();
		Map<String, String> headerMap = new HashMap<String, String>();
		Map<String, String> systemConfigDetails = null;
		HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
		AuthDetailsModel authDetails = null;

		LOGGER.info("RequestToPayServiceImpl in RequestToPayService function adapter request : " + request);

		try {

			// get last token
			authModel = transactionDao.getAuthtokenByCountryCode(request.getCountryCode());

			// check null token
			if (authModel == null) {
				authResponse = authService.createToken(request.getCountryCode());
				if (authResponse.getMessage() != null) {
					response.setCode(authResponse.getCode());
					response.setMessage(authResponse.getMessage());
					response.setMfsTransactionId(request.getExternalId());
					return response;
				}
				authModel = transactionDao.getAuthtokenByCountryCode(request.getCountryCode());
			}

			int tokenExpiredTime = CommonConstant.expirationTime;
			int tokenTime = tokenExpirationCheck.invalidSession(authModel);

			// check whether token expire or not
			if (tokenTime >= tokenExpiredTime) {
				authResponse = authService.createToken(request.getCountryCode());
				if (authResponse.getMessage() != null) {
					response.setCode(authResponse.getCode());
					response.setMessage(authResponse.getMessage());
					return response;
				}
				authModel = transactionDao.getAuthtokenByCountryCode(request.getCountryCode());
			}

			TransactionLogModel transactionLog = transactionDao.getTransactionLogByExtrernalId(request.getExternalId());

			// check for duplicate transaction
			if (transactionLog != null) {
				response.setCode(MFSMtnMomoConstant.ER210);
				response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER210));
			} else {
				systemConfigDetails = mtnMoMoSystemConfigDao.getConfigDetailsMap();
				if (systemConfigDetails == null) {
					response.setCode(MFSMtnMomoConstant.ER237);
					response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER237));
					return response;
				}
				Payee payee = new Payee();
				payee.setPartyId(request.getPayee().getPartyId());
				payee.setPartyIdType(systemConfigDetails.get(CommonConstant.PARTY_ID_TYPE));
				request.setPayee(payee);

				// generate reference Id
				UUID uuid = UUID.randomUUID();
				String referenceId = uuid.toString();

				// log request
				logRequest(request, referenceId);

				connectionRequest.setHttpmethodName("POST");

				// check port null or not
				if (null != systemConfigDetails.get(CommonConstant.PORT)) {
					connectionRequest.setPort(Integer.parseInt(systemConfigDetails.get(CommonConstant.PORT)));
				}
				connectionRequest.setServiceUrl(systemConfigDetails.get(CommonConstant.BASE_URL)
						+ systemConfigDetails.get(CommonConstant.TRANSFER_URL));

				// get header values from auth details table
				authDetails = transactionDao.getAuthDetailsByCountryCode(request.getCountryCode());

				if (null == authDetails) {
					response.setCode(MFSMtnMomoConstant.ER242);
					response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER242));
					return response;
				}

				// set header
				headerMap.put(CommonConstant.REFERENCE_ID, referenceId);
				headerMap.put(CommonConstant.AUTHORIZATION, CommonConstant.BEARER + authModel.getAccessToken());

//				headerMap.put(CommonConstant.CALLBACK_URL, "https://" + authDetails.getxCallbackUrl());
				headerMap.put(CommonConstant.CALLBACK_URL, authDetails.getxCallbackUrl());

				headerMap.put(CommonConstant.TARGET_ENVIRONMENT, authDetails.getxTargetEnvironment());
				headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
				headerMap.put(CommonConstant.SUBSCRIPTION_KEY, authDetails.getSubscriptionKey());
				connectionRequest.setHeaders(headerMap);

				String requestParam = gson.toJson(request);

				// log event request
				eventRequestLog(requestParam, request.getExternalId());

				LOGGER.info("RequestToPayServiceImpl in requestToPay function partner request : " + requestParam);

				httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(requestParam, connectionRequest);

				LOGGER.info("RequestToPayServiceImpl in requestToPay function partner response : " + httpsConResult);

				// check null result
				if (httpsConResult == null) {
					response.setCode(MFSMtnMomoConstant.ER213);
					response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER213));
					response.setMfsTransactionId(request.getExternalId());
					return response;
				}

				// event response log
				eventResponseLog(httpsConResult.getCode());

				if (null != httpsConResult.getTxMessage()
						&& httpsConResult.getTxMessage().equals(MomoCodes.ER401.getMessage())) {

					authResponse = authService.createToken(request.getCountryCode());
					if (authResponse.getMessage() != null) {
						response.setCode(authResponse.getCode());
						response.setMessage(authResponse.getMessage());
						return response;
					}

					// set new token
					headerMap.put(CommonConstant.AUTHORIZATION, CommonConstant.BEARER + authResponse.getAccess_token());

					connectionRequest.setHeaders(headerMap);

					LOGGER.info("RequestToPayServiceImpl in requestToPay function partner request : "
							+ gson.toJson(request));

					httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(requestParam, connectionRequest);

					LOGGER.info(
							"RequestToPayServiceImpl in requestToPay function partner response : " + httpsConResult);

					// check null result
					if (httpsConResult == null) {
						response.setCode(MFSMtnMomoConstant.ER213);
						response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER213));
						response.setMfsTransactionId(request.getExternalId());
						return response;
					}
					// event response log
					eventResponseLog(httpsConResult.getCode());
				}

				updateLogResponse(httpsConResult.getRespCode(), request.getExternalId(), httpsConResult);

				if ((httpsConResult.getRespCode() != MomoCodes.S202.getCode())) {
					logError(request, httpsConResult);
					response.setCode(httpsConResult.getCode());
					response.setMessage(httpsConResult.getTxMessage());
				} else {
					response.setCode(httpsConResult.getRespCode() + "");
					response.setMessage(httpsConResult.getTxMessage());
				}
			}
			response.setMfsTransactionId(request.getExternalId());

			LOGGER.info("RequestToPayServiceImpl in RequestToPayService function adapter response : " + response);

		} catch (DaoException de) {
			LOGGER.error("==>DaoException in RequestToPayServiceImpl", de);
			response.setCode(de.getStatus().getStatusCode());
			response.setMessage(de.getStatus().getStatusMessage());
			response.setMfsTransactionId(request.getExternalId());
		} catch (Exception e) {
			LOGGER.error("==>Exception in RequestToPayServiceImpl", e);
			response.setCode(MFSMtnMomoConstant.ER203);
			response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER203));
			response.setMfsTransactionId(request.getExternalId());
		}
		return response;
	}

	private void eventResponseLog(String response) throws DaoException {
		MtnMomoEventLog transEventLog = transactionDao.getByLastRecord();
		if (transEventLog != null) {
			transEventLog.setResponse(response);
			transEventLog.setDateLogged(new Date());
			transactionDao.update(transEventLog);
		}
	}

	private void eventRequestLog(String requestParam, String mfsId) throws DaoException {
		MtnMomoEventLog eventRequestLog = new MtnMomoEventLog();
		eventRequestLog.setMfsId(mfsId);
		eventRequestLog.setRequest(requestParam);
		eventRequestLog.setServiceName(CommonConstant.REQUEST_TO_PAY_SERVICE);
		eventRequestLog.setDateLogged(new Date());
		transactionDao.save(eventRequestLog);
	}

	// to log request
	private void logRequest(RequestToPayRequestDto request, String referenceId) throws DaoException {
		TransactionLogModel transactionLogModel = new TransactionLogModel();
		transactionLogModel.setAmount(request.getAmount());
		transactionLogModel.setCurrency(request.getCurrency());
		transactionLogModel.setExternalId(request.getExternalId());
		transactionLogModel.setPayerPartyIdType(request.getPayee().getPartyIdType());
		transactionLogModel.setPayerPartyId(request.getPayee().getPartyId());
		if (null != request.getPayerMessage()) {
			transactionLogModel.setPayerMessage(request.getPayerMessage());
		}
		if (null != request.getPayeeNote()) {
			transactionLogModel.setPayeeNote(request.getPayeeNote());
		}
		transactionLogModel.setReferenceId(referenceId);
		transactionLogModel.setCountryCode(request.getCountryCode());
		transactionLogModel.setDate(new Date());
		transactionDao.save(transactionLogModel);
	}

	// to update response
	private void updateLogResponse(int responseCode, String externalId, HttpsConnectionResponse httpsConResult)
			throws DaoException {
		TransactionLogModel transactionLog = transactionDao.getTransactionLogByExtrernalId(externalId);
		if (transactionLog != null) {
			transactionLog.setCode(Integer.toString(responseCode));
			transactionLog.setMessage(httpsConResult.getTxMessage());
			transactionLog.setDate(new Date());
			transactionDao.update(transactionLog);
		}
	}

	// to log error response
	private void logError(RequestToPayRequestDto request, HttpsConnectionResponse httpsConResult) throws DaoException {
		TransactionErrorModel transactionError = new TransactionErrorModel();
		transactionError.setAmount(request.getAmount());
		transactionError.setCurrency(request.getCurrency());
		transactionError.setExternalId(request.getExternalId());
		transactionError.setPayerPartyIdType(request.getPayee().getPartyIdType());
		transactionError.setPayerPartyId(request.getPayee().getPartyId());
		if (null != request.getPayerMessage()) {
			transactionError.setPayerMessage(request.getPayerMessage());
		}
		if (null != request.getPayeeNote()) {
			transactionError.setPayeeNote(request.getPayeeNote());
		}
		transactionError.setTxnErrorCode(httpsConResult.getCode());
		transactionError.setTxnErrorMessage(httpsConResult.getTxMessage());
		transactionError.setDate(new Date());
		transactionDao.save(transactionError);
	}

}