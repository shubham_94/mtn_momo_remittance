package com.mfs.client.mtn.momo.service;

import com.mfs.client.mtn.momo.dto.RequestToPayRequestDto;
import com.mfs.client.mtn.momo.dto.RequestToPayResponseDto;

public interface RequestToPayService {

	public RequestToPayResponseDto requestToPay(RequestToPayRequestDto request);

}