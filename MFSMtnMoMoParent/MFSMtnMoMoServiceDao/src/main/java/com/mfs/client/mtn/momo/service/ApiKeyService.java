package com.mfs.client.mtn.momo.service;

import com.mfs.client.mtn.momo.dto.ApiKeyResponseDto;

public interface ApiKeyService {

	public ApiKeyResponseDto createApiKey();
	
}
