package com.mfs.client.mtn.momo.util;

public enum MomoCodes {
	
	S200(200 , "OK"),
	S201(201 ,"User Created"),
	S202(202 , "Accepted"),
	ER400(400 ,"Bad Request"),
	ER401(401 , "Unauthorized"),
	ER404(404 , "Not Found"),
	ER405(405 , "Method not supported"),
	ER409(409 , "Conflict"),
	ER500(500,"Internal Server Error"), ER403(403,"Forbidden IP");
	
	private int code;
	private String message;

	private MomoCodes(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

}
