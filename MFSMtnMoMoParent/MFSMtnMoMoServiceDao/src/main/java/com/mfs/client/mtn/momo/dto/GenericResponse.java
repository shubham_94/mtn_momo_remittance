package com.mfs.client.mtn.momo.dto;

/**
 * 
 */


import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_DEFAULT)
public class GenericResponse {

	private String mfsTransactionId;
	private String responseCode;
	private String etransactionId;
	private String message;
	private Map<String, String> additionalParameter = new HashMap<String, String>();
	
	public String getMfsTransactionId() {
		return mfsTransactionId;
	}
	public void setMfsTransactionId(String mfsTransactionId) {
		this.mfsTransactionId = mfsTransactionId;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getEtransactionId() {
		return etransactionId;
	}
	public void setEtransactionId(String etransactionId) {
		this.etransactionId = etransactionId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Map<String, String> getAdditionalParameter() {
		return additionalParameter;
	}
	public void setAdditionalParameter(Map<String, String> additionalParameter) {
		this.additionalParameter = additionalParameter;
	}
	@Override
	public String toString() {
		return "GenericResponse [mfsTransactionId=" + mfsTransactionId + ", responseCode=" + responseCode
				+ ", etransactionId=" + etransactionId + ", message=" + message + ", additionalParameter="
				+ additionalParameter + "]";
	}

	
	
}
