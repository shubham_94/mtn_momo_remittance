package com.mfs.client.mtn.momo.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "momo_event_log")
public class MtnMomoEventLog {

	@Id
	@GeneratedValue
	@Column(name = "event_id")
	private int eventId;

	@Column(name = "date_logged")
	private Date dateLogged;

	@Column(name = "service_name")
	private String serviceName;

	@Column(name = "mfs_id")
	private String mfsId;

	@Column(name = "request" , columnDefinition="LONGTEXT")
	private String request;

	@Column(name = "response" , columnDefinition="LONGTEXT")
	private String response;

	@Override
	public String toString() {
		return "MMEventLog [eventId=" + eventId + ", dateLogged=" + dateLogged + ", serviceName=" + serviceName
				+ ", mfsId=" + mfsId + ", request=" + request + ", response=" + response + "]";
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getMfsId() {
		return mfsId;
	}

	public void setMfsId(String mfsId) {
		this.mfsId = mfsId;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

}
