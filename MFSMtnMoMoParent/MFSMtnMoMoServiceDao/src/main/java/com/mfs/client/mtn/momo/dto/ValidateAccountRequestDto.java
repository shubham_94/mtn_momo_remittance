package com.mfs.client.mtn.momo.dto;

public class ValidateAccountRequestDto {
	
	private String accountHolderId;
	private String accountHolderIdType;
	
	public String getAccountHolderId() {
		return accountHolderId;
	}
	public void setAccountHolderId(String accountHolderId) {
		this.accountHolderId = accountHolderId;
	}
	public String getAccountHolderIdType() {
		return accountHolderIdType;
	}
	public void setAccountHolderIdType(String accountHolderIdType) {
		this.accountHolderIdType = accountHolderIdType;
	}
	@Override
	public String toString() {
		return "ValidateAccountRequestDto [accountHolderId=" + accountHolderId + ", accountHolderIdType="
				+ accountHolderIdType + "]";
	}
	
}
