package com.mfs.client.mtn.momo.dto;

public class RequestToPayResponseDto {

	private String code;
	private String message;
	private String mfsTransactionId;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMfsTransactionId() {
		return mfsTransactionId;
	}

	public void setMfsTransactionId(String mfsTransactionId) {
		this.mfsTransactionId = mfsTransactionId;
	}

	@Override
	public String toString() {
		return "RequestToPayResponseDto [code=" + code + ", message=" + message + ", mfsTransactionId="
				+ mfsTransactionId + "]";
	}

}
