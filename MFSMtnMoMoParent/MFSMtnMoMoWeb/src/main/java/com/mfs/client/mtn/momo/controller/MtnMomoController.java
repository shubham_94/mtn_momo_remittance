package com.mfs.client.mtn.momo.controller;

import java.util.Properties;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.client.mtn.momo.dto.ApiKeyResponseDto;
import com.mfs.client.mtn.momo.dto.BalanceResponseDto;
import com.mfs.client.mtn.momo.dto.GetRequestToPayResponseDto;
import com.mfs.client.mtn.momo.dto.GetUserResponseDto;
import com.mfs.client.mtn.momo.dto.RequestToPayRequestDto;
import com.mfs.client.mtn.momo.dto.RequestToPayResponseDto;
import com.mfs.client.mtn.momo.dto.TokenResponseDto;
import com.mfs.client.mtn.momo.dto.UserRequestDto;
import com.mfs.client.mtn.momo.dto.UserResponse;
import com.mfs.client.mtn.momo.dto.ValidateAccountResponseDto;
import com.mfs.client.mtn.momo.service.AccountHolderService;
import com.mfs.client.mtn.momo.service.ApiKeyService;
import com.mfs.client.mtn.momo.service.AuthorizationTokenService;
import com.mfs.client.mtn.momo.service.BalanceEnquiryService;
import com.mfs.client.mtn.momo.service.GetRequestToPayService;
import com.mfs.client.mtn.momo.service.GetUserService;
import com.mfs.client.mtn.momo.service.RequestToPayService;
import com.mfs.client.mtn.momo.service.UserService;
import com.mfs.client.mtn.momo.util.CommonValidations;
import com.mfs.client.mtn.momo.util.MFSMtnMomoConstant;

@RestController
public class MtnMomoController {

	@Autowired
	UserService userService;

	@Autowired
	RequestToPayService requestToPayService;

	@Autowired
	GetUserService getUserService;

	@Autowired
	AccountHolderService accountHolderService;

	@Autowired
	ApiKeyService apiKeyService;

	@Autowired
	AuthorizationTokenService authorizationTokenService;

	@Autowired
	BalanceEnquiryService balanceEnquiryService;

	@Autowired
	GetRequestToPayService getRequestToPayService;

	@Resource(name = "responseC")
	private Properties responseCodes;

	private static final Logger LOGGER = Logger.getLogger(MtnMomoController.class);

	@PostMapping(value = "/userapi")
	public UserResponse userService(@RequestBody UserRequestDto requestDto) {
		UserResponse response = null;
		CommonValidations commonValidation = new CommonValidations();

		if (commonValidation.validateStringValues(requestDto.getProviderCallbackHost())) {
			response = new UserResponse();
			response.setCode(MFSMtnMomoConstant.ER227);
			response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER227));
		} else {
			response = userService.userService(requestDto);
		}

		return response;
	}

	@RequestMapping(value = "/getuserapi")
	public GetUserResponseDto getUserService() {

		GetUserResponseDto response = getUserService.getUserService();
		return response;
	}

	@RequestMapping(value = "/apikey")
	public ApiKeyResponseDto createApiKey() {

		ApiKeyResponseDto apiKeyResponseDto = apiKeyService.createApiKey();
		return apiKeyResponseDto;

	}

	// Auth
	@RequestMapping(value = "/token/{countryCode}", method = RequestMethod.GET)
	public TokenResponseDto createAuthToken(@PathVariable("countryCode") String countryCode) {
		TokenResponseDto tokenResponseDto = authorizationTokenService.createToken(countryCode);
		return tokenResponseDto;
	}

	// request to pay (Transfer)
	@RequestMapping(value = "/requesttopay", method = RequestMethod.POST)
	public RequestToPayResponseDto requestToPay(@RequestBody RequestToPayRequestDto request) {

		RequestToPayResponseDto response = new RequestToPayResponseDto();
		CommonValidations commonValidation = new CommonValidations();
		if (commonValidation.validateStringValues(request.getExternalId())) {
			response.setCode(MFSMtnMomoConstant.ER230);
			response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER230));
			response.setMfsTransactionId(request.getExternalId());
		} else if (commonValidation.validateAmount(request.getAmount())) {
			response.setCode(MFSMtnMomoConstant.ER228);
			response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER228));
			response.setMfsTransactionId(request.getExternalId());
		} else if (commonValidation.validateForCountryCode(request.getCurrency())) {
			response.setCode(MFSMtnMomoConstant.ER229);
			response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER229));
			response.setMfsTransactionId(request.getExternalId());
		} else if (commonValidation.validateStringValues(request.getPayee().getPartyId())) {
			response.setCode(MFSMtnMomoConstant.ER232);
			response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER232));
			response.setMfsTransactionId(request.getExternalId());
		} else if (commonValidation.validateStringValues(request.getCountryCode())) {
			response.setCode(MFSMtnMomoConstant.ER241);
			response.setMessage(responseCodes.getProperty(MFSMtnMomoConstant.ER241));
			response.setMfsTransactionId(request.getExternalId());
		} else {
			response = requestToPayService.requestToPay(request);
		}
		return response;
	}

	// Get request to pay (status)
	@RequestMapping(value = "/getrequesttopay/{externalId}")
	public GetRequestToPayResponseDto getRequestToPay(@PathVariable("externalId") String externalId) {
		GetRequestToPayResponseDto response = getRequestToPayService.getRequestToPay(externalId);
		return response;
	}

	// balance
	@RequestMapping(value = "/getbalanceapi/{countryCode}", method = RequestMethod.GET)
	public BalanceResponseDto balanceResponse(@PathVariable("countryCode") String countryCode) {
		BalanceResponseDto response = new BalanceResponseDto();
		response = balanceEnquiryService.balanceService(countryCode);
		return response;
	}

	// account holder
	@RequestMapping(value = "/getaccountholder/{msisdn}/{countryCode}")
	public ValidateAccountResponseDto getAccountHolder(@PathVariable("msisdn") String msisdn,
			@PathVariable("countryCode") String countryCode) {
		ValidateAccountResponseDto response = accountHolderService.getAccountHolderService(msisdn, countryCode);
		return response;
	}

	// callback
	@RequestMapping(value = "/callback", method = RequestMethod.POST)
	public void callback(@RequestBody String callbackRequest) {
		LOGGER.info("callback service request " + callbackRequest);
	}

}
